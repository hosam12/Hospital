<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Contact;
use App\Notifications\ContactNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::guard('user')->check()){
            if ((Auth::user()->setting->vedio=='Visible')&&(Auth::user()->general->vedio=='Visible')){

            $contacts=Auth::user()->contact()->where('tybesender','user')->get();
            return view('cms.user.contacts.index',['contacts'=>$contacts]);
        }

            else
            return redirect()->route('user.dashbord');
        }
        if (Auth::guard('admin')->check()) {
            $contacts = Auth::user()->contact()->where('tybesender', 'admin')->get();
            return view('cms.admin.contacts.index', ['contacts' => $contacts]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(Auth::guard('user')->check()){
                        if ((Auth::user()->setting->vedio=='Visible')&&(Auth::user()->general->vedio=='Visible')){

            return view('cms.user.contacts.create');}
            else{
                return redirect()->route('user.dashbord');
            }
        }
        if(Auth::guard('admin')->check()){
            $userid=0;
            $users=User::all();
            return view('cms.admin.contacts.create',['users'=>$users,'userid'=>$userid]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request->get('resever'));
        if(Auth::guard('user')->check()){
            if ((Auth::user()->setting->vedio=='InVisible')||(Auth::user()->general->vedio=='InVisible')){
             Alert::warning('ليس لديك صلاحية', 'فشلت العملية');
             return redirect()->route('user.dashbord');

            }
        }
                $request->validate([
                    'subject'=>'required|string|max:45',
                    'articale'=>'required|string',
                    'image'=>'image',
                ],[

                    'subject.required'=>'العنوان فارغ',
                    'subject.max'=>'يجب  تقليل عدد حروف العنوان',
                    'articale.required'=>'الموضوع فارغ',
                    'image.image'=>'يجب أن تكون صورة'
                ]);
                $contact=new Contact();
                $contact->subject=$request->get('subject');
                $contact->articale=$request->get('articale');
                 if($request->hasFile('image')){
                 $imagefile=$request->file('image');
                 $imagename=time().' '.$request->name.' '.' '.$imagefile->getClientOriginalName();

                 $imagefile->move('images/contacts',$imagename);
                 $contact->image=$imagename ;
                 }
                 if(Auth::guard('user')->check()){

                $contact->tybesender='user';
                $contact->tyberesever='admin';
                $contact->user_id=Auth::user()->id;
                $contact->admin_id=1;




                 }
                 else{
                    if(!($request->resever)){
                             $request->session()->flash('status','alert-danger');
                             $request->session()->flash('message','الرجاء اختيار مسوق');
                             return redirect()->back()->withInput();
                    }
                     $contact->tybesender='admin';
                     $contact->tyberesever='user';
                     $contact->user_id=$request->resever;
                     $contact->admin_id=Auth::user()->id;

                 }

                 $save=$contact->save();
                 if(Auth::guard('user')->check()){

                    $admins=Admin::all();
                    $name='رسالة جديدة';
                    $note=new ContactNotification($contact,$name);
                    for ($i=0; $i <count($admins) ; $i++) {

                        $admins[$i]->notify($note);
                        }

                 }
                 else
                 {


                     $user=User::findOrFail($request->resever);
                     $name='رسالة جديدة';

                     $note=new ContactNotification($contact,$name);
                     $user->notify($note);
                 }

                 if($save){
                     $request->session()->flash('status','alert-success');
                     $request->session()->flash('message','تم إرسال الرسالة بنجاح');
                    return redirect()->back();
                 }





    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        if(Auth::guard('user')->check()){
    if(!((Auth::user()->setting->vedio=='InVisible')||(Auth::user()->general->vedio=='InVisible'))){
            return view('cms.user.contacts.show',['contact'=>Contact::find($id)]);
        }}

        if (Auth::guard('admin')->check()) {
            // dd(45);
            return view('cms.admin.contacts.show', ['contact' => Contact::find($id)]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        if(Auth::guard('admin')->check()){


        $request->validate([
            'replay'=>'required|string'
        ],[
            'replay.required'=>'الرد فارغ'
        ]);
            // dd(33223);
        $contact=Contact::with('user')->findOrFail($id);
        $contact->replay=$request->get('replay');
        $save=$contact->save();
        if($save){
            $user=$contact->user;
            $name='رد جديد';
            $note=new ContactNotification($contact,$name);
            $user->notify($note);

        Alert::success('تم إضافة الرد  بنجاح', 'نجحت العملية');
        return redirect()->back();
        }}


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(Auth::guard('admin')->check()){


           $del=Contact::destroy($id);
           if ($del){
           return response()->json(['icon'=>'success','title'=>'  تم الحذف بنجاح'],200);
           }else{
           return response()->json(['icon'=>'error','title'=>'Deleted Post failed'],400);
           }
        }

    }

    public function selectuser($id){

          $users=User::all();
        return view('cms.admin.contacts.create',['users'=>$users,'userid'=>$id]);
    }
    public function contactresever(){

        if(Auth::guard('user')->check()){
        $contacts=Auth::user()->contact()->where('tybesender','admin')->get();
        return view('cms.user.contacts.index',['contacts'=>$contacts]);
        }
        if (Auth::guard('admin')->check()) {
            $contacts = Auth::user()->contact()->where('tybesender', 'user')->get();
            return view('cms.admin.contacts.index', ['contacts' => $contacts]);
        }

    }
}
