<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

use function Ramsey\Uuid\v1;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::guard('user')->check()){
                 if(Auth::user()->setting->category=='Visible'&&Auth::user()->general->category=='Visible'){
                     $cate=Category::where('status','Visible')->withcount(['product'])->with('product')->get();
            return view('cms.user.categorys.index',['cate'=>$cate]);
        }
        }
        elseif(Auth::guard('admin')->check()){


        $cate= Category::withCount(['product'])->get();
        // dd($cate[0]->product_count);
        return view('cms.admin.categorys.index',['categorys'=>$cate]);}
    }

    public function CategoryProduct($id){
        if(Auth::guard('admin')->check()){
     $products=Category::findOrfail($id)->product()->paginate(15);
     return view('cms.admin.products.index',['products'=>$products]);
        }
        if(Auth::guard('user')->check()){
                 if(Auth::user()->setting->product=='Visible'&&Auth::user()->general->product=='Visible'){

             $products=Category::findOrfail($id)->product()->get();
             return view('cms.user.products.index',['products'=>$products]);}
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(Auth::guard('admin')->check()){
        return view('cms.admin.categorys.create');

        }
           else
           return redirect()->route('user.dashbord');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        //
        if(Auth::guard('guardName')->check()){


          $request->validate([

            'name'=>'required|string|min:5',
            'status'=>'string|in:on',
            'image'=>'required|image',
          ],[

                'name.required'=>'اسم الفئة مطلوب',
                'name.min'=>'أحرف الفئة أقل من 5 ',
                'image.required'=>'الرجاء رفع صورة',
                'image.image'=>'يجب أن تكون صورة',

          ]);

          $cate=new Category();
          $cate->name=$request->get('name');
            $cate->desc=$request->get('desc');
          $cate->status=$request->status =='on'?'Visible':'InVisible';
          if($request->hasFile('image')){
          $imagefile=$request->file('image');
          $imagename=time().' '.$request->name.' '.' '.$imagefile->getClientOriginalName();

          $imagefile->move('images/categorys',$imagename);
          $cate->image=$imagename ;
          }
          $save= $cate->save();
          if($save){
         Alert::success('تم الإنشاء بنجاح', 'نجحت العملية');
            return redirect()->back();
             }

            }
                else
                return redirect()->route('user.dashbord');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if(Auth::guard('admin')->check()){


        $cate=Category::findOrFail($id);
        return view('cms.admin.categorys.edit',['category'=>$cate]);
        }
            else
            return redirect()->route('user.dashbord');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if(Auth::guard('admin')->check()){


         $request->validate([

         'name'=>'required|string|min:5',
         'status'=>'string|in:on',
         'image'=>'image',
         ],[

         'name.required'=>'اسم الفئة مطلوب',
         'name.min'=>'أحرف الفئة أقل من 5 ',
         'image.image'=>'يجب أن تكون صورة',

         ]);

         $cate=Category::findOrFail($id);
         $cate->name=$request->get('name');
        $cate->status=$request->status =='on'?'Visible':'InVisible';
        $cate->desc=$request->get('desc');

         if($request->hasFile('image')){
         $imagefile=$request->file('image');
         $imagename=time().' '.$request->name.' '.' '.$imagefile->getClientOriginalName();

         $imagefile->move('images/categorys',$imagename);
         $cate->image=$imagename ;
         }
         $save= $cate->save();
         if($save){
         Alert::success('تم التعديل بنجاح', 'نجحت العملية');
         return redirect()->back();
         }
        }
            else
            return redirect()->route('user.dashbord');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::guard('admin')->check()){


        $products=Category::findOrFail($id)->product;
        foreach($products as $prod){
            Product::findOrFail($prod->id)->delete();


        }
        $del=Category::findOrfail($id)->delete();
              if ($del){
              return response()->json(['icon'=>'success','title'=>'تم الحذف بنجاح  '],200);
              }else{
              return response()->json(['icon'=>'error','title'=>'Deleted Post failed'],400);
              }
            }
                else
                return redirect()->route('user.dashbord');

    }
}
