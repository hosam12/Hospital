<?php

namespace App\Http\Controllers;

use App\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if(Auth::guard('admin')->check()){
        $user=User::with(['setting'])->findOrFail($id);
            $setting=$user->setting;
          $setting->create_order=$request->get('create_order')=='on'?'Visible':'InVisible';
          $setting->index_order=$request->get('index_order')=='on'?'Visible':'InVisible';
          $setting->product=$request->get('product')=='on'?'Visible':'InVisible';
          $setting->category=$request->get('category')=='on'?'Visible':'InVisible';
          $setting->vedio=$request->get('vedio')=='on'?'Visible':'InVisible';
           $user->status=$request->get('status')=='on'?'Active':'Blocked';
             $usersave=$user->save();
          $setSave= $setting->save();
          if($setSave==$usersave){
          Alert::success('تم التعديل بنجاح', 'نجحت العملية');
          return redirect()->back();
          }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
