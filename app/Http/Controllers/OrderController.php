<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Notifications\OrderNotification;
use App\Notifications\ProductNotification;
use App\Order;
use App\OrderProduct;
use App\Product;
use App\User;
use GuzzleHttp\Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        // $details=OrderProduct::with('order.user')->first();
        // dd($details->order);


         if(Auth::guard('user')->check()){

                     if(Auth::user()->setting->index_order=='Visible'&&Auth::user()->general->index_order=='Visible'){
         $orders=Order::with('details')->withcount('details')->where('user_id',Auth::user()->id)->orderByDesc('created_at')->get();
         return view('cms.user.orders.index',['orders'=>$orders]); }
         else
         return redirect()->route('user.dashbord');
         }
           if(Auth::guard('admin')->check()){
         $orders= Order::with(['details','user'])->withCount('details')->orderByDesc('created_at')->get();
         return view('cms.admin.orders.index',['orders'=>$orders]);}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         if (Auth::user()->setting->create_order=='Visible'&&Auth::user()->general->create_order=='Visible'){
        return view('cms.user.orders.create');

         }
        else
        return redirect()->route('user.dashbord');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    if (Auth::user()->setting->create_order=='Visible'&&Auth::user()->general->create_order=='Visible'){

        $request->validate([
            'name'=>'required|string|min:8',
            'emara'=>'required|string',
            'address'=>'required|string|min:9',
            'mobile'=>'required|numeric|min:6',

            'whats'=>'required|numeric|min:6',


        ],[
        'name.required'=>'اسم الزبون مطلوب',
        'name.min'=>'اسم الزبون أقل من 8 أحرف',
        'emara.required'=>'اسم الإمارة مطلوب',
        'address.required'=>'العنوان مطلوب',
        'address.min'=>'يجب أن يتجاوز العنوان 9 أحرف',
        'mobile.required'=>'الهاتف مطلوب',
        'mobile.min'=>'يجب أن يتجاوز الهاتف 6 أحرف',
        'whats.required'=>'رقم الواتس مطلوب',
        'whats.min'=>'يجب أن يتجاوز رقم الواتس 6 أحرف',
        'whats.numeric'=>'التأكد من رقم الواتس',
        'mobile.numeric'=>'التأكد من رقم الهاتف',





        ]);

        $request->validate([      'code1.required'=>'كود المنتج الأول مطلوب',
            'code1.exists'=>'الكود الذي أدخلته للمنتج الأول غير موجود ',
            'price1.required'=>'سعر القطعة الواحدة للمنتج الأول مطلوب',
            'count1.required'=>'عدد القطع للمنتج الأول مطلوبة',
            'image1.image'=>'الملف المطلوب هو صورة للمنتج الأول',
            'image1.required'=>'ارفق صورة المنتج الأول',
            'code1'=>'required|string|exists:products,code',
            'price1'=>'required|integer',
            'count1'=>'required|integer|min:1',
            'image1'=>'required|image',
        ],[



        ]);

        $product1=Product::where('code',$request->code1)->first();
        if($product1->from){
            if(!($request->price1>=$product1->from&&$request->price1<=$product1->to)){

                      $request->session()->flash('status','alert-danger');
                      $request->session()->flash('message','المبلغ المطلوب وضعه في المنتج الأول هو بين '.$product1->from.' إلى'.$product1->to);
                      return redirect()->route('order.create')->withInput();
            }
        }
        else{
    if($request->price1!=$product1->staticprice){
        $request->session()->flash('status','alert-danger');
        $request->session()->flash('message','سعر المنتج الأول ثابت ولا يمكن وضع أقل من '.$product1->staticprice
        );
        return redirect()->route('order.create')->withInput(); }

        }
        ///////////////////////////////////////////////////////////////
        if($request->code2||$request->price2||$request->count2||$request->image2||$request->note2){

            $request->validate([
                'code2' => 'required|string|exists:products,code',
                'price2' => 'required|integer',
                'count2' => 'required|integer|min:1',
                'image2' => 'required|image',

            ], [

                'code2.required' => 'كود المنتج الثاني مطلوب',
                'code2.exists' => 'الكود الذي أدخلته للمنتج الثاني غير موجود ',
                'price2.required' => 'سعر القطعة الواحدة للمنتج الثاني مطلوب',
                'count2.required' => 'عدد القطع للمنتج الثاني مطلوبة',
                'image2.image' => 'الملف المطلوب هو صورة للمنتج الثاني',
                'image2.required' => 'ارفق صورة المنتج الثاني',

            ]);


            $product2 = Product::where('code', $request->code2)->first();
            if ($product2->from) {
                if (!($request->price1 >= $product2->from && $request->price1 <= $product2->to)) {

                    $request->session()->flash('status', 'alert-danger');
                    $request->session()->flash('message', 'المبلغ المطلوب وضعه في المنتج الثاني هو بين ' . $product2->from . ' إلى' . $product2->to);
                    return redirect()->route('order.create')->withInput();
                }
            } else {
                if ($request->price2 != $product2->staticprice) {
                    $request->session()->flash('status', 'alert-danger');
                    $request->session()->flash('message', 'سعر المنتج الثاني ثابت ولا يمكن وضع أقل من ' . $product2->staticprice);
                    return redirect()->route('order.create')->withInput();
                }
            }

        }

            ///////////////////////////////////////////////////////////////
            if($request->code3||$request->price3||$request->count3||$request->image3||$request->note3){

            $request->validate([
            'code3' => 'required|string|exists:products,code',
            'price3' => 'required|integer',
            'count3' => 'required|integer|min:1',
            'image3' => 'required|image',

            ], [

            'code3.required' => 'كود المنتج الثاني مطلوب',
            'code3.exists' => 'الكود الذي أدخلته للمنتج الثاني غير موجود ',
            'price3.required' => 'سعر القطعة الواحدة للمنتج الثاني مطلوب',
            'count3.required' => 'عدد القطع للمنتج الثاني مطلوبة',
            'image3.image' => 'الملف المطلوب هو صورة للمنتج الثاني',
            'image3.required' => 'ارفق صورة المنتج الثاني',

            ]);


            $product3 = Product::where('code', $request->code3)->first();
            if ($product3->from) {
            if (!($request->price3 >= $product3->from && $request->price3 <= $product3->to)) {

                $request->session()->flash('status', 'alert-danger');
                $request->session()->flash('message', 'المبلغ المطلوب وضعه في المنتج الثالث هو بين ' . $product3->from .
                ' إلى' . $product3->to);
                return redirect()->route('order.create')->withInput();
                }
                } else {
                if ($request->price3 != $product3->staticprice) {
                $request->session()->flash('status', 'alert-danger');
                $request->session()->flash('message', 'سعر المنتج الثالث ثابت ولا يمكن وضع أقل من ' .
                $product3->staticprice);
                return redirect()->route('order.create')->withInput();
                }
                }

                }
                    ///////////////////////////////////////////////////////////////
                    if($request->code4||$request->price4||$request->count4||$request->image4||$request->note4){

                    $request->validate([
                    'code4' => 'required|string|exists:products,code',
                    'price4' => 'required|integer',
                    'count4' => 'required|integer|min:1',
                    'image4' => 'required|image',

                    ], [

                    'code4.required' => 'كود المنتج الرابع مطلوب',
                    'code4.exists' => 'الكود الذي أدخلته للمنتج الرابع غير موجود ',
                    'price4.required' => 'سعر القطعة الواحدة للمنتج الرابع مطلوب',
                    'count4.required' => 'عدد القطع للمنتج الرابع مطلوبة',
                    'image4.image' => 'الملف المطلوب هو صورة للمنتج الرابع',
                    'image4.required' => 'ارفق صورة المنتج الرابع',

                    ]);


                    $product4 = Product::where('code', $request->code4)->first();
                    if ($product4->from) {
                    if (!($request->price4 >= $product4->from && $request->price4 <= $product4->to)) {

                        $request->session()->flash('status', 'alert-danger');
                        $request->session()->flash('message', 'المبلغ المطلوب وضعه في المنتج الرابع هو بين ' .
                        $product4->from . ' إلى' . $product4->to);
                        return redirect()->route('order.create')->withInput();
                        }
                        } else {
                        if ($request->price4 != $product4->staticprice) {
                        $request->session()->flash('status', 'alert-danger');
                        $request->session()->flash('message', 'سعر المنتج الرابع ثابت ولا يمكن وضع أقل من ' .
                        $product4->staticprice);
                        return redirect()->route('order.create')->withInput();
                        }
                        }

                        }




                         ///////////////////////////////////////////////////////////////
                         if($request->code5||$request->price5||$request->count5||$request->image5||$request->note5){

                         $request->validate([
                         'code5' => 'required|string|exists:products,code',
                         'price5' => 'required|integer',
                         'count5' => 'required|integer|min:1',
                         'image5' => 'required|image',

                         ], [

                         'code5.required' => 'كود المنتج الرابع مطلوب',
                         'code5.exists' => 'الكود الذي أدخلته للمنتج الرابع غير موجود ',
                         'price5.required' => 'سعر القطعة الواحدة للمنتج الرابع مطلوب',
                         'count5.required' => 'عدد القطع للمنتج الرابع مطلوبة',
                         'image5.image' => 'الملف المطلوب هو صورة للمنتج الرابع',
                         'image5.required' => 'ارفق صورة المنتج الرابع',

                         ]);


                         $product5 = Product::where('code', $request->code5)->first();
                         if ($product5->from) {
                         if (!($request->price5 >= $product5->from && $request->price5 <= $product5->to)) {

                             $request->session()->flash('status', 'alert-danger');
                             $request->session()->flash('message', 'المبلغ المطلوب وضعه في المنتج الخامس هو بين ' .
                             $product5->from . ' إلى' . $product5->to);
                             return redirect()->route('order.create')->withInput();
                             }
                             } else {
                             if ($request->price5 != $product5->staticprice) {
                             $request->session()->flash('status', 'alert-danger');
                             $request->session()->flash('message', 'سعر المنتج الخامس ثابت ولا يمكن وضع أقل من ' .
                             $product5->staticprice);
                             return redirect()->route('order.create')->withInput();
                             }
                             }

                             }
                             $ordertotal=0;
                             $allprofit1=0;
                             $allprofit2=0;
                             $allprofit3=0;
                             $allprofit4=0;
                             $allprofit5=0;
                             $profit1=0;
                             $profit2=0;
                             $profit3=0;
                             $profit4=0;
                             $profit5=0;

                             $allprofit1=($request->price1-$product1->realprice)*$request->count1;
                             $profit1=($allprofit1*Auth::user()->userprice)/100;
                             $ordertotal=$ordertotal+$request->count1*$request->price1;
                             ///////////////
                             if($request->code2){
                            $allprofit2=($request->price2-$product2->realprice)*$request->count2;
                            $profit2=($allprofit2*Auth::user()->userprice)/100;
                            $ordertotal=$ordertotal+$request->count2*$request->price2;

                             }

                                  //////////////
                                  if($request->code3){
                            $allprofit3=($request->price3-$product3->realprice)*$request->count3;
                            $profit3=($allprofit3*Auth::user()->userprice)/100;
                            $ordertotal=$ordertotal+$request->count3*$request->price3;

                                  }

                                       ///////////
                                       if($request->code4){
                    $allprofit4=($request->price4-$product4->realprice)*$request->count5;
                    $profit4=($allprofit4*Auth::user()->userprice)/100;
                    $ordertotal=$ordertotal+$request->count4*$request->price4;

                                                }

                                             /////////////

                                             if($request->code5){


                            $allprofit5=($request->price5-$product5->realprice)*$request->count5;
                            $profit5=($allprofit5*Auth::user()->userprice)/100;
                                $ordertotal=$ordertotal+$request->count5*$request->price5;

                                                    }
                                $order = new Order();
                                $order->name=$request->get('name');
                                $order->emara=$request->get('emara');
                                $order->address=$request->get('address');
                                $order->mobile=$request->get('mobile');
                                $order->whats=$request->get('whats');

                                $order->total=$ordertotal;
                                $order->totalprofit=$profit1+$profit2+$profit3+$profit4+$profit5;
                                $order->user_id=Auth::user()->id;
                       $ordersave=      $order->save();
                       if($ordersave){
                    $admins=Admin::all();
                    $name='تم إضافة طلبية جديدة  ';
                    $note=new OrderNotification($order,$name);
                        for ($i=0; $i <count($admins) ; $i++) {
                    $admins[$i]->notify($note);
                        }
                        // foreach($authors as $author){
                        // for ($i=0; $i <count($admins) ; $i++) { $admins[$i]->notify($note);
                        //     }

                        if($request->code1){
                            $orderproduct=new OrderProduct();
                            $orderproduct->order_id=$order->id;
                            $orderproduct->product_id=$product1->id;
                            $orderproduct->count=$request->count1;
                            $orderproduct->total=$request->count1*$request->price1;
                            if($request->hasFile('image1')){
                                  $imagefile=$request->file('image1');
                                  $imagename=time().' '.$request->code1.' '.' '.$imagefile->getClientOriginalName();

                                  $imagefile->move('images/orders',$imagename);
                                  $orderproduct->image=$imagename ;
                            }
                            $orderproduct->currentPrice=$request->get('price1');
                            $orderproduct->currentuserprice=Auth::user()->userprice;
                            $orderproduct->note=$request->get('note1');
                            $orderproduct->profit=$profit1;
                            $save=$orderproduct->save();


                        }
                        /////////////////////////////////////////////////////
                                        if($request->code2){
                                        $orderproduct=new OrderProduct();
                                        $orderproduct->order_id=$order->id;
                                        $orderproduct->product_id=$product2->id;
                                        $orderproduct->count=$request->count2;
                                        $orderproduct->total=$request->count2*$request->price2;
                                        if($request->hasFile('image2')){

                                        $imagefile=$request->file('image2');

                                        $imagename=time().' '.$request->code2.''. $imagefile->getClientOriginalName();


                                        $imagefile->move('images/orders',$imagename);
                                        $orderproduct->image=$imagename ;
                                        }
                                        $orderproduct->currentPrice=$request->get('price2');
                                        $orderproduct->currentuserprice=Auth::user()->userprice;
                                        $orderproduct->note=$request->get('note2');
                                        $orderproduct->profit=$profit2;
                                        $save=$orderproduct->save();


                                        }
                                        /////////////////////////////////////////////////////
                                        if($request->code3){
                                        $orderproduct=new OrderProduct();
                                        $orderproduct->order_id=$order->id;
                                        $orderproduct->product_id=$product3->id;
                                        $orderproduct->count=$request->count3;
                                        $orderproduct->total=$request->count3*$request->price3;
                                        if($request->hasFile('image3')){
                                        $imagefile=$request->file('image3');
                                        $imagename=time().' '.$request->code3.' '.''.$imagefile->getClientOriginalName();

                                        $imagefile->move('images/orders',$imagename);
                                        $orderproduct->image=$imagename ;
                                        }
                                        $orderproduct->currentPrice=$request->get('price3');
                                        $orderproduct->currentuserprice=Auth::user()->userprice;
                                        $orderproduct->note=$request->get('note3');
                                        $orderproduct->profit=$profit3;
                                        $save=$orderproduct->save();


                                        }
                                               /////////////////////////////////////////////////////
                                               if($request->code4){
                                               $orderproduct=new OrderProduct();
                                               $orderproduct->order_id=$order->id;
                                               $orderproduct->product_id=$product4->id;
                                               $orderproduct->count=$request->count4;
                                               $orderproduct->total=$request->count4*$request->price4;
                                               if($request->hasFile('image4')){
                                               $imagefile=$request->file('image4');
                                               $imagename=time().' '.$request->code4.' '.$imagefile->getClientOriginalName();

                                               $imagefile->move('images/orders',$imagename);
                                               $orderproduct->image=$imagename ;
                                               }
                                               $orderproduct->currentPrice=$request->get('price4');
                                               $orderproduct->currentuserprice=Auth::user()->userprice;
                                               $orderproduct->note=$request->get('note4');
                                               $orderproduct->profit=$profit4;
                                               $save=$orderproduct->save();


                                               }
                                                       /////////////////////////////////////////////////////
                                                       if($request->code5){
                                                       $orderproduct=new OrderProduct();
                                                       $orderproduct->order_id=$order->id;
                                                       $orderproduct->product_id=$product5->id;
                                                       $orderproduct->count=$request->count5;
                                                       $orderproduct->total=$request->count5*$request->price5;
                                                       if($request->hasFile('image5')){
                                                       $imagefile=$request->file('image5');
                                                       $imagename=time().' '.$request->code5.' '.''.$imagefile->getClientOriginalName();

                                                       $imagefile->move('images/orders',$imagename);
                                                       $orderproduct->image=$imagename ;
                                                       }
                                                       $orderproduct->currentPrice=$request->get('price5');
                                                       $orderproduct->currentuserprice=Auth::user()->userprice;
                                                       $orderproduct->note=$request->get('note5');
                                                       $orderproduct->profit=$profit5;
                                                       $save=$orderproduct->save();


                                                       }

                       }
                       if($save){
                            Alert::success('تم إنشاء الطلبية بنجاح', 'نجحت العملية');
                           return redirect()->back();

                       }



                    }
                    else
                    return redirect()->route('user.dashbord');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::guard('user')->check()){
            if(Auth::user()->setting->index_order=='Visible'&&Auth::user()->general->index_order=='Visible'){

            $order= $order=Order::with(['details.product'=>function($query){
               $query= $query->with(['category'=>function($query){
                   $query->withTrashed()->get();
               }])->withTrashed()->get();
            }])->withcount('details')->orderByDesc('created_at')->findOrFail($id);
            if($order->user_id==Auth::user()->id){

            return view('cms.user.orders.show',['order'=>$order]);

            }
        else{
            return redirect()->route('user.dashbord');
        }

    }
        }
        if(Auth::guard('admin')->check()){


         $order=Order::with(['details.product'=>function($query){
        $query= $query->with(['category'=>function($query){
            $query->withTrashed()->get();
        }])->withTrashed()->get();

         }])->withcount('details')->orderByDesc('created_at')->findOrFail($id);


            $totalrealprice=0;
            foreach($order->details->where('status','success') as $s){
                $totalrealprice=$totalrealprice+($s->count*$s->product->realprice);
            }

        return view('cms.admin.orders.show',['order'=>$order,'totalrealprice'=>$totalrealprice]);}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $order=Order::findOrfail($id);

                    foreach($order->details as $item){

                    $item->delete();
                    }
                    $del=$order->delete();

 if ($del){
          return response()->json(['icon'=>'success','title'=>' تم حذف الطلبية بنجاح'],200);
          }else{
          return response()->json(['icon'=>'error','title'=>'Deleted Post failed'],400);
          }
    }

    public function editstatus(Request $request ,$id) {
        $order=Order::findOrFail($id);
        $order->status=$request->get('status');
        if($request->get('status')=='cancel'){
        foreach($order->details as $item){
            $item->status=$request->get('cancel');
            $item->save();
        }
        }

      $save=$order->save();
        if($save){
         Alert::success('تم تعديل الطلبية بنجاح', 'نجحت العملية');
         return redirect()->back();
        }

    }

}
