<?php

namespace App\Http\Controllers;
use App\Notifications\ProductNotification;

use App\Category;
use App\Image;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

use function Ramsey\Uuid\v1;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        // $products=Product::with(['category'=>function($query){
        //     $query->withTrashed();
        // }])->get();

            if(Auth::guard('user')->check()){
                // dd(Auth::user()->seeting);
             if(Auth::user()->setting->product=='Visible'&&Auth::user()->general->product=='Visible'){

            $products=Product::where('status','Visible')->with('category')->get();

            return view('cms.user.products.index',['products'=>$products]);
         }
         else
         return redirect()->route('user.dashbord');
            }
           if(Auth::guard('admin')->check()){


        $products=Product::with('category')->paginate(15);

        return view('cms.admin.products.index',['products'=>$products]);
            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(Auth::guard('admin')->check()){


        $cate=Category::all();
        return view('cms.admin.products.create',['cate'=>$cate]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {




    //   dd(count($request->file('image')) );

        if(Auth::guard('admin')->check()){
        $request->validate([
        'name'=>'required|string|min:3|max:45',
        'desc'=>'nullable|string',
        'code'=>'required|string|unique:products,code',
        'realprice'=>'required|integer|min:1',
        'to'=>'nullable|integer',
        'from'=>'nullable|integer',
        'category'=>'required|integer',
        'note'=>'nullable|string',
        'gender'=>'string|in:Male,Female,MaleFemale',
        'status'=>'string|in:on',
        'color'=>'nullable|string',
        'staticprice'=>'nullable|integer',



        ],[
            'name.required'=>'اسم المنتج مطلوب',
            'category.required'=>'الرجاء إختيار فئة المنتج',
            'name.min'=>'المنتج يجب أن يتجاوز ثلاث حروف',
            'name.max'=>'عدد حروف المنتج أكبر من 45 حرف',
            'code.unique'=>'كود المنتج موجود مسبقاً',
            'code.required'=>'كود المنتج مطلوب',
            'realprice.required'=>'الرجاء كتابة سعر المنتج من التاجر',
            'realprice.min'=>'يجب أن يكون سعر المنتج من التاجر أكبر من 1',



        ]);

        $t=false;
        if($request->get('to')){
            $t=true;
        }
        $s=false;
        if($request->get('staticprice')){
        $s=true;
        }
        $f=false;
        if($request->get('from')){


            $f=true;
        }
        if((($f&&$t)==true)&&($s==false)){
            $request->validate([
                'from'=>'integer',
                'to'=>'integer',
            ]);
            if(!($request->get('from')>$request->get('realprice')&&$request->get('to')>$request->get('from'))){
               if($request->get('from')<$request->get('realprice')){
                      $request->session()->flash('status','alert-danger');
                      $request->session()->flash('message','يجب أن يكون الحدالأدنى أكبر من سعر التاجر');
                      return redirect()->route('product.create')->withInput();
               }
            else if($request->get('to')<$request->get('from')){
                   $request->session()->flash('status','alert-danger');
                   $request->session()->flash('message','يجب أن يكون الحد الأقصى أكبر من الحد الأدنى');
                   return redirect()->route('product.create')->withInput();
            }
            }


        }
        elseif((($f||$t)==false)&&($s==true)){
            if(!($request->get('staticprice')>$request->get('realprice'))){
                  $request->session()->flash('status','alert-danger');
                  $request->session()->flash('message','يجب أن يكون السعر الثابت أكبر من سعر التاجر');
                  return redirect()->route('product.create')->withInput();
            }
        }
        else{
             $request->session()->flash('status','alert-danger');
             $request->session()->flash('message','الرجاء تحديد السعر أو وضع سعر ثابت');
             return redirect()->route('product.create')->withInput();

        }
        $product= new Product();
        $product->name=$request->get('name');
        $product->code=$request->get('code');
        $product->category_id=$request->get('category');
        $product->desc=$request->get('desc');
        $product->realprice=$request->get('realprice');
        $product->from=$request->get('from');
        $product->to=$request->get('to');
        $product->staticprice=$request->get('staticprice');
        $product->color=$request->get('color');
        $product->gender=$request->get('gender');
        $product->status=$request->get('status')=='on'?'Visible':'InVisible';
        $product->note=$request->get('note');
     $save=   $product->save();
     if($save){
          foreach ($request->file('image',[]) as $im) {

          $h= new Image();
          $imagefile=$im;
          $imagename=time().'_'.$request->name.'_'.'_'.$imagefile->getClientOriginalName();
          $h->image=$imagename;

          $imagefile->move('images/products',$imagename);
          $h->object_type='App\Category';
          $h->object_id=1;
        $product->images()->save($h);



          }



         if($product->status=='Visible'){
   $users=User::all();
   $name='تم إضافة منتج جديد ';
   $note=new ProductNotification($product,$name);
   // foreach($authors as $author){
   for ($i=0; $i <count($users) ; $i++) {
       $users[$i]->notify($note);
       }
         }

         Alert::success('تم الإنشاء بنجاح', 'نجحت العملية');
         return redirect()->back();
     }}





    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::guard('admin')->check()){


        $product=Product::with(['category','images'])->findOrFail($id);

        $cate=Category::all();
        return view('cms.admin.products.edit',['product'=>$product,'cate'=>$cate]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        if(Auth::guard('admin')->check()){
        $request->validate([
        'name'=>'required|string|min:3|max:45',
        'desc'=>'nullable|string',
        'code'=>'required|string',
        'realprice'=>'required|integer|min:1',
        'to'=>'nullable|integer',
        'from'=>'nullable|integer',
        'category'=>'required|integer',
        'note'=>'nullable|string',
        'gender'=>'string|in:Male,Female,MaleFemale',
        'status'=>'string|in:on',
        'color'=>'nullable|string',
        'staticprice'=>'nullable|integer',



        ],[
        'name.required'=>'اسم المنتج مطلوب',
        'category.required'=>'الرجاء إختيار فئة المنتج',
        'name.min'=>'المنتج يجب أن يتجاوز ثلاث حروف',
        'name.max'=>'عدد حروف المنتج أكبر من 45 حرف',
        'code.unique'=>'كود المنتج موجود مسبقاً',
        'code.required'=>'كود المنتج مطلوب',
        'realprice.required'=>'الرجاء كتابة سعر المنتج من التاجر',
        'realprice.min'=>'يجب أن يكون سعر المنتج من التاجر أكبر من 1',



        ]);
        $e=Product::where('id','!=',$id)->get();
        $t=$e->where('code',$request->code)->first();
        if($t){
                $request->session()->flash('status','alert-danger');
                $request->session()->flash('message','كود المنتج موجود مسبقاً');
                return redirect()->back()->withInput();

        }

        $t=false;
        if($request->get('to')){
        $t=true;
        }
        $s=false;
        if($request->get('staticprice')){
        $s=true;
        }
        $f=false;
        if($request->get('from')){


        $f=true;
        }
        if((($f&&$t)==true)&&($s==false)){
        $request->validate([
        'from'=>'integer',
        'to'=>'integer',
        ]);
        if(!($request->get('from')>$request->get('realprice')&&$request->get('to')>$request->get('from'))){
        if($request->get('from')<$request->get('realprice')){
            $request->session()->flash('status','alert-danger');
            $request->session()->flash('message','يجب أن يكون الحدالأدنى أكبر من سعر التاجر');
            return redirect()->route('product.edit')->withInput();
            }
            else if($request->get('to')<$request->get('from')){
                $request->session()->flash('status','alert-danger');
                $request->session()->flash('message','يجب أن يكون الحد الأقصى أكبر من الحد الأدنى');
                return redirect()->route('product.edit')->withInput();
                }
                }


                }
                elseif((($f||$t)==false)&&($s==true)){
                if(!($request->get('staticprice')>$request->get('realprice'))){
                $request->session()->flash('status','alert-danger');
                $request->session()->flash('message','يجب أن يكون السعر الثابت أكبر من سعر التاجر');
                return redirect()->route('product.edit')->withInput();
                }
                }
                else{
                $request->session()->flash('status','alert-danger');
                $request->session()->flash('message','الرجاء تحديد السعر أو وضع سعر ثابت');
                return redirect()->route('product.edit')->withInput();

                }
                $product= Product::findOrFail($id);
                $product->name=$request->get('name');
                $product->code=$request->get('code');
                $product->category_id=$request->get('category');
                $product->desc=$request->get('desc');
                $product->realprice=$request->get('realprice');
                $product->from=$request->get('from');
                $product->to=$request->get('to');
                $product->staticprice=$request->get('staticprice');
                $product->color=$request->get('color');
                $product->gender=$request->get('gender');
                $product->status=$request->get('status')=='on'?'Visible':'InVisible';
                $product->note=$request->get('note');
                $save= $product->save();
                if($save){
                    if($request->hasFile('image')){

                     foreach ($request->file('image',[]) as $im) {

                     $h= new Image();
                     $imagefile=$im;
                     $imagename=time().'_'.$request->name.'_'.'_'.$imagefile->getClientOriginalName();
                     $h->image=$imagename;

                     $imagefile->move('images/products',$imagename);
                     $h->object_type='App\Category';
                     $h->object_id=1;
                     $product->images()->save($h);



                     }}
                         if($product->status=='Visible'){
                         $users=User::all();
                         $name='تم تعديل المنتج ';
                         $note=new ProductNotification($product,$name);
                         // foreach($authors as $author){
                         for ($i=0; $i <count($users) ; $i++) {
                              $users[$i]->notify($note);
                             }
                             }
                Alert::success('تم التعديل بنجاح', 'نجحت العملية');
                return redirect()->route('product.index');
                }}

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(Auth::guard('admin')->check()){

              $del=Product::findOrfail($id)->delete();
              if ($del){
              return response()->json(['icon'=>'success','title'=>'Deleted Post successfuly'],200);
              }else{
              return response()->json(['icon'=>'error','title'=>'Deleted Post failed'],400);
              }
        }


    }
    public function getcode(Request $request){

            $product=Product::where('code',$request->code)->first();

            $realprice=$product->realprice;

            if($realprice){
          return response()->json(['icon'=>$realprice],200);

            }
            else{
             return response()->json(['icon'=>'notfound'],400);

            }
    }
}
