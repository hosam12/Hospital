<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $admin= new Admin();
        $admin->name='hossam';
        $admin->email='hm@gmail.com';
        $admin->age=20;
        $admin->numberid=323232328;
        $admin->mobile='+9725928520263';
        $admin->view_password='Pass123$';
        $admin->password=Hash::make($admin->view_password);
        $admin->image='fwfsda';
        $admin->address='ewgwegewgweg';
        $admin->urlfase='4343434';
        $admin->urlwats='eewewe';
        $admin->urlinsta='ogjeoje';
        $IsSave= $admin->save();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if($id==Auth::user()->id){


        return view('cms.admin.profile');}
        else
         return redirect()->route('admin.dashbord');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|string|min:10|max:45',
            'mobile'=>'required|string|min:6',
            'numberid'=>'required|integer|min:100000000|max:999999999|unique:users,email',
            'urlfase'=>'string',
            'urlinsta'=>'string',
            'urlwats'=>'string',
            'age'=>'required|min:18|integer',
            'address'=>'required|string|min:10|max:45',
            'image'=>'image',
            'now'=>'string|max:15',


        ],[

            'name.required'=>'الاسم مطلوب',
          'mobile.required'=>'الهاتف مطلوب',
          'mobile.min'=>'الرجاء كتابة رقم الهاتف بالشكل الصحيح',
          'name.min'=>'أحرف الاسم أقل من 10 ',
          'name.max'=>'أحرف الاسم أكبر من 45 ',
          'image.image'=>'يجب أن تكون صورة',
            'address.required'=>'العنوان مطلوب',
          'address.min'=>'أحرف العنوان أقل من 10',
          'address.max'=>'أحرف العنوان أكبر من 45 ',
          'age.required'=>'العمر مطلوب',
          'age.min'=>'يجب أن يتجاوز المدير  18 عام ',
          'gender.required'=>'الرجاء أختيار الجنس',
            'numberid.max'=>'رقم الهوية يجب أن يكون 9 أرقام',
            'numberid.min'=>'رقم الهوية يجب أن يكون 9 أرقام',
            'numberid.required'=>'رقم الهوية مطلوب',
            'now.string'=>'يجب أن تكون النبذة نص',
            // 'now.min'=>'النبذة أقل من 10 حروف',
            'now.max'=>'النبذة أكثر من من 15 حرف الرجاء التقليل'



        ]);
         $e=Admin::where('id','!=',$id)->get();
         $t=$e->where('numberid',$request->numberid)->first();
         if($t){
         $request->session()->flash('status','alert-danger');
         $request->session()->flash('message','هذا الحساب مسجل من قبل');
         return redirect()->back();

         }

         $admin=Admin::findOrfail($id);
         $admin->name=$request->get('name');
         $admin->numberid=$request->get('numberid');
         $admin->urlfase=$request->get('urlfase');
         $admin->urlinsta=$request->get('urlinsta');
         $admin->urlwats=$request->get('urlwats');
         $admin->age=$request->get('age');
         $admin->now=$request->get('now');
         $admin->address=$request->get('address');
         $admin->gender=$request->get('gender');
         if($request->hasFile('image')){
         $imagefile=$request->file('image');
         $imagename=time().' '.$request->name.' '.' '.$imagefile->getClientOriginalName();

         $imagefile->move('images/admins',$imagename);
         $admin->image=$imagename ;
         }
         $save=$admin->save();
         if($save){
              Alert::success('تم التعديل بنجاح', 'نجحت العملية');
              return redirect()->back();
         }



        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        //
    }
    public function password(Request $request ,$id){

        $request->validate([
            'password'=>'required|string|min:8',

        ],[
            'password.required'=>'كلمة السر مطلوبة',
            'password.min'=>'كلمة السر يجب أن تتجاوز 8 حروف',

        ]);
           if($request->password){
           $request->validate([
           'confirm'=>'required|string|min:8|same:password',
           ],[

           'confirm.required'=>'تأكيد كلمة السر مطلوب',
           'confirm.same'=>'كلمة السر ليست متطابقة',

           ]);
           }
     $admin=Admin::findOrFail($id);
     $admin->view_password=$request->get('password');
     $admin->password=Hash::make($request->get('password'));
       $save=$admin->save();
       if($save){
       Alert::success('تم التعديل بنجاح', 'نجحت العملية');
       return redirect()->back();
       }

    }
}
