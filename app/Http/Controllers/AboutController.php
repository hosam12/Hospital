<?php

namespace App\Http\Controllers;

use App\About;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(Auth::guard('admin')->check()){


            $about=About::first();
            // dd($about);
            if($about){
           return redirect()->route('about.show',[$about->id]);

            }
            else
        return view('cms.admin.abouts.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        if(Auth::guard('admin')->check()){


        $request->validate([
            'articale'=>'required',
            'image'=>'required|image',

        ],[
            'articale.required'=>'النص مطلوب',
            'image.image'=>'يجب وضع صورة',
            'image.required'=>'يجب وضع صورة',
        ]);
            $about=new About();
            $about->articale=$request->get('articale');
             if($request->hasFile('image')){
             $imagefile=$request->file('image');
             $imagename=time().' '.$request->name.' '.' '.$imagefile->getClientOriginalName();

             $imagefile->move('images/abouts',$imagename);
             $about->image=$imagename ;
             }
        $save=$about->save();

        if($save){
            Alert::toast('تم الإنشاء بنجاح', 'success');
            return redirect()->route('about.show',[$about->id]);
        }
    }
       else
       return redirect()->route('user.dashbord');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\About  $about
     * @return \Illuminate\Http\Response
     */
    public function show($id)

    {
        if(Auth::guard('admin')->check()){
        return view('cms.admin.abouts.show',['about'=>About::find($id)]);

        }
        else
        return redirect()->route('user.dashbord');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\About  $about
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::guard('admin')->check()){
  $about=About::first();
  return view('cms.admin.abouts.edit',['about'=>$about]);

        }
           else
           return redirect()->route('user.dashbord');


        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\About  $about
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd(443);
        // dd($request->image);
        if(Auth::guard('admin')->check()){


                $request->validate([
                'articale'=>'required',
                'image'=>'nullable|image',

                ],[
                'articale.required'=>'النص مطلوب',
                // 'image.image'=>'يجب وضع صورة',
                ]);
                $about=About::findOrFail($id);
                $about->articale=$request->get('articale');
                if($request->hasFile('image')){
                $imagefile=$request->file('image');
                $imagename=time().' '.$request->name.' '.' '.$imagefile->getClientOriginalName();

                $imagefile->move('images/abouts',$imagename);
                $about->image=$imagename ;
                }
                $save=$about->save();

                if($save){
                Alert::toast('تم التعديل بنجاح', 'success');
                return redirect()->route('about.show',[$about->id]);
                }
            }
               else
               return redirect()->route('user.dashbord');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\About  $about
     * @return \Illuminate\Http\Response
     */
    public function destroy(About $about)
    {
        //
    }
    public function userabout(){
        $about=About::first();
        return view('cms.user.abouts.show',['about'=>$about]);
    }
}
