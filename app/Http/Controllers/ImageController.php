<?php

namespace App\Http\Controllers;

use App\Image;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if(Auth::guard('user')->check()){
   $product=Product::where('status','Visible')->with('images')->whereHas('category',function($query){
   $query->where('status','Visible');
   })->findOrFail($id);

   return view('cms.user.images.index',['product'=>$product]);
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del=Image::destroy($id);
         if ($del){
         return response()->json(['icon'=>'success','title'=>'تم حذف الصورة بنجاح'],200);
         }else{
         return response()->json(['icon'=>'error','title'=>'Deleted Post failed'],400);
         }
    }
    public function downloadorder($file){
            return response()->download('images/orders/'.$file);
    }
     public function downloadproduct($file){

     return response()->download('images/products/'.$file);
     }
}
