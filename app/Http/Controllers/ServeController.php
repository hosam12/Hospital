<?php

namespace App\Http\Controllers;

use App\Serve;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ServeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $d=Serve::all();
        // dd($d);
        if(Auth::guard('admin')->check()){


        return view('cms.admin.serves.index',['images'=>Serve::all()]);
    }
        if(Auth::guard('user')->check()){
                    return view('cms.user.serves.index',['images'=>Serve::where('status','Visible')->get()]);

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
                if(Auth::guard('admin')->check()){

        if(Auth::guard('admin')->check()){
        return view('cms.admin.serves.create');
        }

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd(32);
                if(Auth::guard('admin')->check()){

        $request->validate([
            'name'=>'required|string|min:5',
            'image'=>'required|image',
            'status'=>'in:on',

        ],[
            'name.required'=>'العنوان مطلوب',
            'name.min'=>'العنوان أفل من 5 حروف',
            'image.image'=>'يجب أن يكون الملف عبارة عن صورة'
        ]);
        $ser=new Serve();
        $ser->name=$request->get('name');
        $ser->status=$request->get('status')=='on'?'Visible':'InVisible';
         if($request->hasFile('image')){
         $imagefile=$request->file('image');
         $imagename=time().' '.$request->name.' '.' '.$imagefile->getClientOriginalName();

         $imagefile->move('images/serves',$imagename);
         $ser->image=$imagename ;
         }
         $save=$ser->save();
         if($save){
                     Alert::success('تم الإنشاء بنجاح', 'نجحت العملية');
                return redirect()->back();
         }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Serve  $serve
     * @return \Illuminate\Http\Response
     */
    public function show(Serve $serve)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Serve  $serve
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
                if(Auth::guard('admin')->check()){

        return view('cms.admin.serves.edit',['serve'=>Serve::find($id)]);
                }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Serve  $serve
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if(Auth::guard('admin')->check()){


           $request->validate([
           'name'=>'required|string|min:5',
           'status'=>'in:on',

           ],[
           'name.required'=>'العنوان مطلوب',
           'name.min'=>'العنوان أفل من 5 حروف',
           ]);
           $ser=Serve::findOrFail($id);
           $ser->name=$request->get('name');
           $ser->status=$request->get('status')=='on'?'Visible':'InVisible';
           if($request->hasFile('image')){
           $imagefile=$request->file('image');
           $imagename=time().' '.$request->name.' '.' '.$imagefile->getClientOriginalName();

           $imagefile->move('images/serves',$imagename);
           $ser->image=$imagename ;
           }
           $save=$ser->save();
           if($save){
           Alert::success('تم التعديل بنجاح', 'نجحت العملية');
           return redirect()->back();
           }}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Serve  $serve
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del=Serve::destroy($id);

         if ($del){
         return response()->json(['icon'=>'success','title'=>'تم الحذف بنجاح '],200);
         }else{
         return response()->json(['icon'=>'error','title'=>'Deleted Post failed'],400);
         }

    }
    public function all(){
        $serves=Serve::all();
        return view('cms.admin.serves.all',['serves'=>$serves]);
    }
}
