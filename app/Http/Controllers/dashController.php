<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Category;
use App\Order;
use App\OrderProduct;
use App\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class dashController extends Controller
{
    //
    public function admin(){
                // Alert::success('successsds', 'success');






                if(Auth::guard('admin')->check()){

                        $now=now();
                        $year=$now->year;
                        $month=$now->month;
                        $oneday=Carbon::create($year,$month,1,0,0,0,'US/Central');
                        // dd($firstday);
                        //lastday in month
                        $last=$now->month()->lastOfMonth()->day;

                        // dd($last);
                        $lastday=Carbon::create($year,$month,$last,23,59,59,'US/Central');


                        $det=OrderProduct::where('created_at','>=',$oneday)->withTrashed()->get();
                        // dd($det);
                        $detailsMonth=$det->where('created_at','<=',$lastday);

                     $details=OrderProduct::withTrashed()->get();

                     $users=User::all();
                     $orders=Order::with('details')->withTrashed()->get();


                     return view('cms.admin.index',[
                    'orders'=>$orders,
                     'details'=>$details,
                     'users'=>$users,
                     'month'=>$detailsMonth,
                     'admins'=>Admin::all()]);
                }


    }


    public function user(){
        if(Auth::guard('user')->check()){



              $now=now();
              $year=$now->year;
              $month=$now->month;
              $oneday=Carbon::create($year,$month,1,0,0,0,'US/Central');
              // dd($firstday);
              //lastday in month
              $last=$now->month()->lastOfMonth()->day;

              // dd($last);
              $lastday=Carbon::create($year,$month,$last,23,59,59,'US/Central');


         $orders=Order::with('details')->withcount('details')->where('user_id',Auth::user()->id)->get();
         $details=OrderProduct::wherehas('order',function($query){
            $query->where('user_id',Auth::user()->id);

         })->withTrashed()->get();
          $detailsmonth=$details->where('created_at','>=',$oneday);
          $usermonth=$detailsmonth->where('created_at','<=',$lastday);
         $profit=$details->where('status','success')->sum('profit');
    return view('cms.user.index',['orders'=>$orders,'profit'=>$profit,'details'=>$details,'month'=>$usermonth]);
        }
}


    public function userprofile(){
        return view('cms.user.profile');
    }

    public function edituser(Request $request,$id){

        // dd($request->numberid);
           $request->validate([
           'name'=>'required|string|min:10|max:45',
           'mobile'=>'required|numeric',
           'address'=>'required|string|min:10|max:45',
           'age'=>'required|integer|min:18',
           'gender'=>'required|string|in:Male,Female',
           'image'=>'image',
        //    'numberid'=>'min:100000000|max:999999999',

           ],[

           'name.required'=>'الاسم مطلوب',
           'mobile.required'=>'الهاتف مطلوب',
            // 'numberid.min'=>'يجب أن يكون رقم الهوية مكون من 9 ارقام',
            // 'numberid.max'=>'يجب أن يكون رقم الهوية مكون من 9 ارقام',
           'name.min'=>'أحرف الاسم أقل من 10 ',
           'name.max'=>'أحرف الاسم أكبر من 45 ',
           'image.image'=>'يجب أن تكون صورة',
           'address.required'=>'العنوان مطلوب',
           'address.min'=>'أحرف العنوان أقل من 10',
           'address.max'=>'أحرف العنوان أكبر من 45 ',
           'age.required'=>'العمر مطلوب',
           'age.min'=>'يجب أن يكون العمر اكبر من 16 عام',
           'gender.required'=>'الرجاء أختيار الجنس',

           ]);




           $user=User::findOrFail($id);
           $user->name=$request->get('name');
           $user->mobile=$request->get('mobile');
           $user->adress=$request->get('address');
           $user->age=$request->get('age');
           if($request->numberid){

                    if( $request->numberid>999999999||$request->numberid<100000000){

                         $request->session()->flash('status','alert-danger');
                        $request->session()->flash('message','هذا الرقم مستخدم من قبل او لم يتجاوز ال9 ارقام');
                        return redirect()->back()->withInput();
                        }
            $us=User::where('numberid',$request->numberid)->get();
            $ds=$us->where('numberid','!=',Auth::user()->id);
            // dd($ds);
            if(count($ds)>0 ){
                // dd(43);
                 $request->session()->flash('status','alert-danger');
                 $request->session()->flash('message','هذا الرقم مستخدم من قبل او لم يتجاوز ال9 ارقام');
                 return redirect()->back()->withInput();
            }
            // dd(6655);
           }

           $user->numberid=$request->get('numberid');
           $user->gender=$request->get('gender');
           if($request->hasFile('image')){
           $imagefile=$request->file('image');
           $imagename=time().' '.$request->name.' '.' '.$imagefile->getClientOriginalName();

           $imagefile->move('images/users',$imagename);
           $user->image=$imagename ;
           }

           $save= $user->save();
           if($save){

           if($save){
           Alert::success('تم التعديل بنجاح', 'نجحت العملية');
           return redirect()->back();
           }

           }
    }

    public function orderublode(){

        if(Auth::guard('admin')->check()){


         $orders=Order::where('status','ublode')->with(['details','user'])->withCount('details')->orderByDesc('created_at')->get();
         return view('cms.admin.orders.index',['orders'=>$orders]);
    }
      return redirect()->back();
}

    public function detailsmonth(){
    // $details=OrderProduct::with('order.user')->withTrashed()->get();
            $now=now();
            $year=$now->year;
            $month=$now->month;
            $oneday=Carbon::create($year,$month,1,0,0,0,'US/Central');
            // dd($firstday);
            //lastday in month
            $last=$now->month()->lastOfMonth()->day;

            // dd($last);
            $lastday=Carbon::create($year,$month,$last,23,59,59,'US/Central');



            $det=OrderProduct::where('created_at','>=',$oneday)->with(['order'=>function($query){
                $query->withTrashed()->with('user');
            }])->with(['product'=>function($query){
                $query->withTrashed()->get();
            }])->withTrashed()->orderByDesc('created_at')->get();
            // dd($det);
            $detailsMonth=$det->where('created_at','<=',$lastday);
    return view('cms.admin.details.index',['details'=>$detailsMonth]);
    }

//get data for this month
    public function usermonth($id){

     $now=now();
     $year=$now->year;
     $month=$now->month;
     $oneday=Carbon::create($year,$month,1,0,0,0,'US/Central');
     $last=$now->month()->lastOfMonth()->day;
     $lastday=Carbon::create($year,$month,$last,23,59,59,'US/Central');
     if(Auth::guard('admin')->check()){
            $details=User::findOrFail($id)->details()->with(['order'=>function($query){
                $query->withTrashed()->with('user');
            }])->with(['product'=>function($query){
            $query->withTrashed()->get();
            }])->orderByDesc('created_at')->get();
              $det=$details->where('created_at','>=',$oneday);
            $detailsMonth=$det->where('created_at','<=',$lastday);
              return view('cms.admin.details.index',['details'=>$detailsMonth]);

        }

        if(Auth::guard('user')->check()){
             $details=Auth::user()->details()->withTrashed()->with(['product'=>function($query){
                 $query->withTrashed()->get();
             }])->orderByDesc('created_at')->get();
             $det=$details->where('created_at','>=',$oneday);
             $detailsMonth=$det->where('created_at','<=',$lastday);
             return view('cms.user.details.index',['details'=>$detailsMonth]);
        }

    }


    public function searchadmin(Request $request){


            // $users=[];
            // $products=[];
            // $orders=[];
            // $categorys=[];
            if(Auth::guard('admin')->check()){
            $products=Product::search($request->search)->where('status','Visible')->get();
            // $products->whereHas('category',function($query){
            //     $query->where('status','Visible');
            // });
            $categorys=Category::withcount('product')->search($request->search)->get();

            // dd($categorys);
            $users=User::withcount('details')->search($request->search)->get();
            // dd($users[0]->image);

        // dd($products);
            return view('cms.admin.search.index',['users'=>$users,'products'=>$products,'categorys'=>$categorys]);
    }
    }
    public function searchuser(Request $request){

        // dd(45);

        if(Auth::guard('user')->check()){


    $products=Product::with('images')->search($request->search)->whereHas('category',function($query){
    $query->where('status','Visible');
    })->where('status','Visible')->get();

    return view('cms.user.search.index',['products'=>$products]);
    }}
}
