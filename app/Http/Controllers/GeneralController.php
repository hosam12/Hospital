<?php

namespace App\Http\Controllers;

use App\General;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

use function Ramsey\Uuid\v1;

class GeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $general=General::first();
        return view('cms.admin.generals.show',['general'=>$general]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\General  $general
     * @return \Illuminate\Http\Response
     */
    public function show(General $general)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\General  $general
     * @return \Illuminate\Http\Response
     */
    public function edit(General $general)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\General  $general
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
        $general=General::findOrFail($id);
        $general->create_order=$request->get('create_order')=='on'?'Visible':'InVisible';
        $general->index_order=$request->get('index_order')=='on'?'Visible':'InVisible';
        $general->product=$request->get('product')=='on'?'Visible':'InVisible';
        $general->category=$request->get('category')=='on'?'Visible':'InVisible';
                $general->vedio=$request->get('vedio')=='on'?'Visible':'InVisible';

        $save=$general->save();
        if($save){
        Alert::success('تم التعديل بنجاح', 'نجحت العملية');
        return redirect()->back();
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\General  $general
     * @return \Illuminate\Http\Response
     */
    public function destroy(General $general)
    {
        //
    }
}
