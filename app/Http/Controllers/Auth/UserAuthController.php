<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;

class UserAuthController extends Controller
{

  use AuthenticatesUsers;

  public function __construct()
  {
  $this->middleware('guest:user')->except('logout');
  }

  public function showLoginView(){
  return view('cms.user.auth.login');
  }

  public function login(Request $request){

  $request->validate([
  'email'=>'required|email|string|exists:users,email',
  'password'=>'required|string|min:6',
  'remember_me'=>'string|in:on'
  ]);



  $rememberMe = $request->remember_me == 'on' ? true : false;

  $credentials = [
  'email'=>$request->email,
  'password'=>$request->password,
  ];




  if (Auth::guard('user')->attempt($credentials, $rememberMe)){
  $user = Auth::guard('user')->user();
  if ($user->status == "Active"){

  return redirect()->route('user.dashbord');
  }
  else{
  Auth::guard('user')->logout();
  return redirect()->guest(route('user.blocked'));
  }
  }else{
  return redirect()->back()->withInput();
  }
  }

  public function logout(Request $request){
      if(Auth::guard('user')->check()){
Auth::guard('user')->logout();
$request->session()->invalidate();
return redirect()->guest(route('user.login.view'));
      }

  }

    //
}
