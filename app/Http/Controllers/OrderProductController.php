<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderProduct;
use App\Product;
use GuzzleHttp\Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Codec\OrderedTimeCodec;
use RealRashid\SweetAlert\Facades\Alert;

class OrderProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        if(Auth::guard('admin')->check()){
            $details=OrderProduct::with(['order'=>function($query){
                $query->with('user')->withTrashed()->get();
            }])->with(['product'=>function($query){
                $query->withTrashed()->get();
            }])->orderByDesc('created_at')->get();
            return view('cms.admin.details.index',['details'=>$details]);
        }

        if(Auth::guard('user')->check()){
            if(Auth::user()->setting->index_order=='Visible'&&Auth::user()->general->index_order=='Visible'){

            $details=Auth::user()->details()->with(['product'=>function($query){
                $query->withTrashed()->get();
            }])->orderByDesc('created_at')->get();
            return view('cms.user.details.index',['details'=>$details]);
                     }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderProduct  $orderProduct
     * @return \Illuminate\Http\Response
     */
    public function show(OrderProduct $orderProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderProduct  $orderProduct
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
                if(Auth::guard('admin')->check()){

        $details=OrderProduct::with(['order'=>function($query){
            $query->with('user')->withTrashed()->get();
        }])->with(['product'=>function($query){
            $query->withTrashed()->get();
        }])->withTrashed()->findOrFail($id);
        return view('cms.admin.orders.edit',['details'=>$details]);

    }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderProduct  $orderProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    //  dd($cc);
    if(Auth::guard('admin')->check()){
            $cc = Product::where('code', $request->code)->withTrashed()->first();
            $ss=orderProduct::where('id',$id)->withTrashed()->first();

       if(($cc->deleted_at)||($ss->deleted_at)){
            $request->session()->flash('status','alert-danger');
            $request->session()->flash('message','لا يمكن التعديل لأن المنتج محذوف ');
                 Alert::warning('لا يمكن التعديل لأن المنتج محذوف', 'فشلت العملية');
        return redirect()->route('order.index');
       }

        if(Auth::guard('admin')->check()){


        $request->validate([
            'code'=> 'required|string|exists:products,code',
            'price'=>'required|integer',
            'count'=>'required|integer',
            'image'=>'image'

        ],[
                  'code.required'=>'كود المنتج  مطلوب',
                  'code.exists'=>'الكود الذي أدخلته للمنتج  غير موجود ',
                  'price.required'=>'سعر القطعة الواحدة للمنتج  مطلوب',
                  'count.required'=>'عدد القطع للمنتج  مطلوبة',
                  'image.image'=>'الملف المطلوب هو صورة للمنتج ',
        ]);
// dd($request->code);
$details=OrderProduct::with('order')->findOrFail($id);
// dd($details);
$product=Product::where('code',$request->get('code'))->first();
$details->product_id=$product->id;

$details->currentPrice=$request->get('price');
$details->count=$request->get('count');




  if($request->hasFile('image')){
  $imagefile=$request->file('image');
  $imagename=time().' '.$request->code1.' '.' '.$imagefile->getClientOriginalName();

  $imagefile->move('images/orders',$imagename);
  $details->order->image-=$imagename ;

  }
  $details->total=$request->get('count')*$request->get('price');
  $priceman=$request->get('count')*$product->realprice;
  $allprofit=$details->total-$priceman;
  $details->profit=($allprofit*$details->currentuserprice)/100;


  if(($request->status=='success')&&($details->status!='success')){
//   dd(0);
  $user=$details->order->user;
  $user->mall= $details->order->user->mall+$details->profit;
  $user->save();
  }
  if(($request->status=='cancel'||$details->status=='waiting')&&$details->status=='success')
  {
  $user=$details->order->user;
  $user->mall= $user->mall-$details->profit;
  $user->save();
  }
  if($request->status){
  $details->status=$request->get('status');

  }

  $save=$details->save();
//   dd(0);


  if($save){

          Alert::success('تم تعديل الطلبية بنجاح', 'نجحت العملية');
          return redirect()->back();
  }
    }
}


}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderProduct  $orderProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderProduct $orderProduct)
    {
        //
    }

    public function detailsdata(Request $request){

        // dd(3323);
        if(($request->from)&&($request->to)){
           $from=Carbon::create($request->from,0,0,0,'US/Central');
        //    dd($to);
           $data=Carbon::create($request->to,0,0,0,'US/Central');
        $m=$data->month;
        $d= $data->day;
         $y=$data->year;
            $to=Carbon::create($y,$m,$d,23,59,59,'US/Central');
            // dd($to);
            if(Auth::guard('user')->check()){
                // dd(22);
                ////////////////////////////////
                ////////////////////////////////
                /////////////////////////
                ////////////////////////////
                $details=OrderProduct::whereHas('order',function($query){
                    $query->withTrashed();
                    $query->where('user_id',Auth::user()->id);
                })->with(['order'=>function($query){
                    $query->withTrashed()->get();
                }])->withTrashed()->with(['product'=>function($query){
                    $query->withTrashed()->get();
                }])->get();
                ////////////////////
                ////////////////////
                ////////////////////
                ////////////////////






            // $details=OrderProduct::whereHas('order',function($query){
            //     $query=$query->withTrashed()->get();
            //     $query->where('user_id','1');

            // })->withTrashed()->get();



            $sd= $details->where('created_at','>=',$from);

            $details=$sd->where('created_at','<=',$to);

            return view('cms.user.details.index',['details'=>$details]);
        }
        if(Auth::guard('admin')->check()){
            // dd(333);
                //    $details=OrderProduct::withTrashed()->with('order')->withTrashed()->with('user')->withTrashed()->get();
                // $details=OrderProduct::withTrashed()->with(['order'=>function($query){
                //     $query->with('user')->withTrashed()->get();
                //     dd($query->user);
                // }])->with(['product'=>function($query){
                //     $query->withTrashed()->get();
                // }])->orderByDesc('created_at')->get();
                $details=OrderProduct::with(['order'=>function($query){
                    $query->with('user')->withTrashed()->get();
                }])->with(['product'=>function($query){
                    $query->withTrashed()->get();
                }])->withTrashed()->get();

                   $sd= $details->where('created_at','>',$from);

                   $details=$sd->where('created_at','<',$to); return view('cms.admin.details.index',['details'=>
                       $details]);

        }


        }
        else{

            Alert::warning(' الرجاء وضع التاريخ بشكل صحيح', ' فشلت العملية');
            return redirect()->back();
        }



    //   dd(Order::where('created_at','>',$s,'and','created_at',1)->where('id',1)->get());

    }

}
