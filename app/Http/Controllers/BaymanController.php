<?php

namespace App\Http\Controllers;

use App\Bayman;
use Illuminate\Http\Request;

class BaymanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bayman  $bayman
     * @return \Illuminate\Http\Response
     */
    public function show(Bayman $bayman)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bayman  $bayman
     * @return \Illuminate\Http\Response
     */
    public function edit(Bayman $bayman)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bayman  $bayman
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bayman $bayman)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bayman  $bayman
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bayman $bayman)
    {
        //
    }
}
