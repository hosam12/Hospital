<?php

namespace App;

use Nicolaslopezj\Searchable\SearchableTrait;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    //
    use SearchableTrait;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'products.name' => 10,
            'products.code' => 10,
            'categories.name'=>10,

        ],
        'joins' => [
        'categories' => ['products.category_id', 'categories.id'],
        ],

    ];


    public function images(){

    return $this->morphMany(Image::class,'object');

    }
      public function category(){
      return $this->belongsTo(Category::class,'category_id','id');
      }
      public function order(){
          return $this->belongsToMany(Order::class,OrderProduct::class,'product_id','order_id');
      }


    //    public function authors()
    //    {
    //    return $this->belongsToMany(Author::class, AuthorCategory::class, 'category_id', 'author_id');
    //    }
}
