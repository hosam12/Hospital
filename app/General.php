<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class General extends Model
{
    //
    public  function users(){
        return $this->hasMany(User::class,'general_id','id');
    }
}
