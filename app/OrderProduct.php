<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderProduct extends Model
{
    use SoftDeletes;
    public function product(){
        return $this->belongsTo(Product::class,'product_id','id');

    }
        public function order(){
        return $this->belongsTo(Order::class,'order_id','id');

        }




        // public function category(){
        //     return $this->hasOneThrough(Category::class,Product::class,)
        // }

    //
}
