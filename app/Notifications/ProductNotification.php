<?php

namespace App\Notifications;

use App\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ProductNotification extends Notification
{
    use Queueable;

    private $product;
    private $name;
    public function __construct(Product $product,$name)
    {
        $this->product=$product;
           $this->name=$name;

    }

    public function via($notifiable)
    {
        return ['database'];
    }


    public function toDatabase(){

        return[
      'id'=>$this->product->id,
      'title'=>$this->name,
      'type'=>'product',
      'image'=>$this->product->category->image,
      'category'=>$this->product->category->name,
      'data'=>$this->product->created_at,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
