<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
        use Notifiable;

    //
   public function users()
   {
   return $this->hasMany(User::class, 'admin_id', 'id');
   }
    public function contact(){
    return $this->hasMany(Contact::class,'admin_id','id');
    }
}
