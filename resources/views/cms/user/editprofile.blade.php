@extends('cms.user.parent')
@section('title','تعديل الصفحة الشخصية')

@section('content')

 <div id="main-content">
            <div class="container">
                <div class="block-header">
                    <div class="row clearfix">
                        <div class="col-md-6 col-sm-12">
                            <h2>تعديل البيانات</h2>
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('user.profile',[Auth::user()->id])}}">الصفحة الشخصية</a></li>
                                <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('user.dashbord')}}">الرئيسية</a></li>
                                    <li class="breadcrumb-item active" style="font-size: 20px" aria-current="page">تعديل الصفحة الشخصية</li>
                                </ol>
                            </nav>
                        </div>

                    </div>
                </div>
                <div class="row clearfix">



                    <div class="col-xl-8 col-lg-8 col-md-7">

                        <div class="card">
                        <form action="{{route('edit.profile',[Auth::user()->id])}}" enctype="multipart/form-data" method="POST">
                              @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </div>
                        @endif
                        @if (session()->has('message'))
                                        <div class="alert {{session()->get('status')}} alert-dismissible fade show"
                                             role="alert">
                                            <span> {{ session()->get('message') }}</span>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                         @endif
                            @csrf
                            @method("PUT")

                                <div class="form-group row">
                                    <div class="col-md-10 col-sm-10">

                                        <div class="form-group">
                                            <label style="font-size: 20px" >الإسم</label>
                                        <input type="text" class="form-control" value="{{Auth::user()->name}}" style="font-size: 20px" name="name" placeholder=" ادخل الإسم رباعي ">
                                        </div>
                                        <div class="form-group">
                                            <label style="font-size: 20px" >رقم الهوية</label>
                                            <input type="number"  class="form-control" value="{{Auth::user()->numberid}}" style="font-size: 20px" @if(Auth::user()->numberid) disabled @endif name="numberid"   placeholder="أدخل رقم الهوية">
                                        </div>
                                           <div class="form-group">
                                            <label style="font-size: 20px" > الإيميل</label>
                                            <input type="email"  class="form-control" value="{{Auth::user()->email}}" style="font-size: 20px" name="email" disabled  placeholder="الإيميل الشخصي">
                                        </div>
                                         <div class="form-group">
                                            <label style="font-size: 20px" >رقم الهاتف</label>
                                            <input type="tel" class="form-control" value="{{Auth::user()->mobile}}" style="font-size: 20px" name="mobile"  placeholder="أدخل رقم الهاتف">
                                        </div>
                                         <div class="form-group">
                                            <label style="font-size: 20px" >العنوان</label>
                                            <input type="text" class="form-control" value="{{Auth::user()->adress}}" style="font-size: 20px" name="address" placeholder="ادخل العنوان">
                                        </div>
                                         <div class="form-group">
                                            <label style="font-size: 20px" > العمر</label>
                                            <input type="number" class="form-control" value="{{Auth::user()->age}}" style="font-size: 20px" name="age" placeholder="أدخل العمر">
                                        </div>
                                            <label style="font-size: 20px" > الصورة الشخصية</label>

                                         <div class="row clearfix">
                                            <div class="col-lg-12">
                                                <input type="file" width="590px" height="390px" class="dropify" name="image">
                                            </div>

                                        </div>

                                         <div class="form-group">
                                            <label style="font-size: 20px" >نسبة المسوق بالمئة</label>
                                         <input type="number" disabled class="form-control"  style="font-size: 20px" value="{{Auth::user()->userprice}}" >
                                        </div>

                                    <div class="form-group">
                                <label style="font-size: 20px">الجنس</label>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input"  type="radio" id="Male"
                                        name="gender" value="Male"
                                        @if(Auth::user()->gender== 'Male') checked  @endif>
                                    <label  for="Male" class="custom-control-label" style="font-size: 20px">ذكر</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="Female"
                                        name="gender" value="Female"
                                        @if(Auth::user()->gender == 'Female') checked @endif>
                                    <label for="Female" class="custom-control-label" style="font-size: 20px">أنثى</label>
                                </div>
                            </div>

                                            <div>
                                            <button style="font-size: 20px" class="btn btn-sm btn-primary" type="submit"> تعديل  </button>
                                </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
