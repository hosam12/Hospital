@extends('cms.user.parent')
@section('title','الصفحة الشخصية')
@section('content')
 <div id="main-content">
            <div class="container">
                <div class="block-header">
                    <div class="row clearfix">
                        <div class="col-md-6 col-sm-12">
                            <h2>الصفحة الشخصية </h2>
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('user.dashbord')}}" style="font-size: 20px">الرئيسية</a></li>
                                    <li class="breadcrumb-item"><a href="{{route('order.index')}}" style="font-size: 20px">طلباتي</a></li>
                                </ol>
                            </nav>
                        </div>

                    </div>
                </div>
                <div class="row clearfix">

                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="card">
                            <div class="header">
                                <ul class="header-dropdown dropdown">
                                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>

                                </ul>
                            </div>
                            <div class="body">
                                 <div class="col-md-12">
                        <div class="card social">
                            <div class="profile-header d-flex justify-content-between justify-content-center">
                                <div class="d-flex">
                                    <div class="mr-3">
                                    <img src="{{url('images/users/'.Auth::user()->image)}}" alt="{{Auth::user()->name}}" class="rounded" alt="">
                                    </div>
                                    <div class="details">
                                        <h5 class="mb-0">{{Auth::user()->name}}</h5>
                                        <p class="mb-0"><span style="font-size: 20px">النسبة: <strong>% {{Auth::user()->userprice}} </strong></p>
                                    </div>
                                </div>
                                <div>

                                <a href="{{route('edit.user.view')}}" class="btn btn-primary btn-sm">تعديل</a>
                                </div>
                            </div>
                        </div>
                    </div>
                                <hr>
                                <small class="text-muted">الإيميل الشخصي: </small>
                                <p>{{Auth::user()->email}}</p>
                                <hr>
                                <small class="text-muted">رقم الهاتف: </small>
                                <p>{{Auth::user()->mobile}}</p>
                                <hr>
                                <small class="text-muted">العمر: </small>
                                <p class="m-b-0">{{Auth::user()->age}}</p>
                                <hr>


                            </div>
                        </div>
                    </div>



                    </div>
                </div>
            </div>
        </div>
@endsection
