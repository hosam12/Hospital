@extends('cms.user.parent')
@section('title','طلبية جديدة')

@section('content')
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>طلبية جديدة </h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a  style="font-size: 20px" href="{{route('user.dashbord')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a   style="font-size: 20px" href="{{route('order.index')}}">الطلبيات</a></li>
                            <li class="breadcrumb-item active" style="font-size: 20px" aria-current="page">طلبية جديدة</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2 style="font-size: 20px">بيانات الزبون </h3>
                        </div>
                    <form action="{{route('order.store')}}" method="POST" enctype="multipart/form-data">
                         @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </div>
                        @endif
                        @if (session()->has('message'))
                                        <div class="alert {{session()->get('status')}} alert-dismissible fade show"
                                             role="alert">
                                            <span> {{ session()->get('message') }}</span>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                         @endif
                        @csrf
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="form-group mb-3">
                                        <label style="font-size: 20px" >اسم الزبون</label>
                                    <input type="text" value="{{old('name')}}" name="name" class="form-control" placeholder="اسم الزبون">
                                    </div>
                                </div>
                               <div class="col-md-6">
                                    <div class="form-group mb-3">
                                        <label style="font-size: 20px" >الإمارة</label>
                                    <input type="text" value="{{old('emara')}}" name="emara" class="form-control" placeholder="ادخل  الإمارة">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group mb-3">
                                        <label style="font-size: 20px" >العنوان </label>
                                    <input type="text" value="{{old('address')}}" name="address" class="form-control" placeholder="أدخل العنوان ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group mb-3">
                                        <label style="font-size: 20px" > رقم الهاتف للتواصل</label>
                                    <input type="text" value="{{old('mobile')}}" name="mobile" class="form-control" placeholder="رقم الهاتف">
                                    </div>
                                </div>
                                  <div class="col-md-6">
                                    <div class="form-group mb-3">
                                        <label style="font-size: 20px" >رقم الواتس</label>
                                    <input type="text" value="{{old('whats')}}" name="whats" class="form-control" placeholder="أدخل رقم الواتس">
                                    </div>
                                </div>

                            </div>

                        </div>
                    <div>


                            <hr>
                        <div class="header">
                            <h2 style="font-size: 20px"> تفاصيل المنتج </h3>
                        </div>
                        <div class="body">

                            <div class="row clearfix">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px"  >كود المنتج  </label>
                                    <input  type="text" name="code1" value="{{old('code1')}}"  class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px" >سعر القطعة الواحدة عند البيع </label>
                                    <input value="{{old('price1')}}" type="number" name="price1"    id="price1" class="form-control" >
                                    </div>
                                </div>
                                 <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px" >العدد </label>
                                    <input type="number" name="count1"  id="count1" value="{{old('count1')}}"  class="form-control" placeholder="العدد">
                                    </div>
                                </div>
                                        <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px" >صورة المنتج </label>
                                        <input type="file" name="image1"    class="form-control" >
                                    </div>
                                </div>


                                <div class="col-sm-6">
                                    <div class="form-group mt-3">
                                    <label style="font-size: 20px" >اللون والمقاس   .... </label>

                                    <textarea rows="2" name="note1"  class="form-control no-resize" placeholder=" اللون والمقاس  ">{{old('note1')}}</textarea>
                                    </div>
                                </div>



                            </div>
                                <div class="col-sm-12">
                                    <button type="button" onclick="pr1()" id="btn1" class="btn btn-primary">اضافة منتج جديد</button>
                                </div>
                        </div>
                    </div>
                <div id="pr2" hidden>


                            <hr>
                        <div class="header">
                            <h2 style="font-size: 20px"> تفاصيل المنتج الثاني </h3>
                        </div>
                        <div class="body">

                            <div class="row clearfix">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px"  >كود المنتج  </label>
                                    <input  type="text" id="code2" name="code2" value="{{old('code2')}}"  class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px" >سعر القطعة الواحدة عند البيع </label>
                                    <input value="{{old('price2')}}" type="number" name="price2"    id="price2" class="form-control" >
                                    </div>
                                </div>
                                 <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px" >العدد </label>
                                    <input type="number" name="count2"  id="count2" value="{{old('count2')}}"  class="form-control" placeholder="العدد">
                                    </div>
                                </div>
                                        <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px" >صورة المنتج </label>
                                        <input type="file" name="image2" id="image2"   class="form-control" >
                                    </div>
                                </div>


                                <div class="col-sm-6">
                                    <div class="form-group mt-3">
                                    <label style="font-size: 20px" > اللون والمقاس   .... </label>

                                    <textarea rows="2" id="note2" name="note2"  class="form-control no-resize" placeholder=" اللون والمقاس  ">{{old('note2')}}</textarea>
                                    </div>
                                </div>



                            </div>
                             <div class="col-sm-12">
                                    <button type="button" onclick="pr2()" id="btn2" class="btn btn-primary">اضافة منتج جديد</button>
                                </div>
                        </div>
                </div>

                    <div id="pr3" hidden>


                            <hr>
                        <div class="header">
                            <h2 style="font-size: 20px"> تفاصيل المنتج الثالث </h3>
                        </div>
                        <div class="body">

                            <div class="row clearfix">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px"  >كود المنتج  </label>
                                    <input  type="text" id="code3" name="code3" value="{{old('code3')}}"  class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px" >سعر القطعة الواحدة عند البيع </label>
                                    <input value="{{old('price3')}}" type="number" name="price3"    id="price3" class="form-control" >
                                    </div>
                                </div>
                                 <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px" >العدد </label>
                                    <input type="number" name="count3"  id="count3" value="{{old('count3')}}"  class="form-control" placeholder="العدد">
                                    </div>
                                </div>
                                        <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px" >صورة المنتج </label>
                                        <input type="file" name="image3" id="image3"    class="form-control" >
                                    </div>
                                </div>


                                <div class="col-sm-6">
                                    <div class="form-group mt-3">
                                    <label style="font-size: 20px" > اللون والمقاس   .... </label>

                                    <textarea rows="2" name="note3" id="note3"  class="form-control no-resize" placeholder=" اللون والمقاس  ">{{old('note3')}}</textarea>
                                    </div>
                                </div>



                            </div>
                             <div class="col-sm-12">
                                    <button type="button" onclick="pr3()" id="btn3" class="btn btn-primary">اضافة منتج جديد</button>
                                </div>
                        </div>
                    </div>
                    <div id="pr4" hidden>


                            <hr>
                        <div class="header">
                            <h2 style="font-size: 20px"> تفاصيل المنتج الرابع </h3>
                        </div>
                        <div class="body">

                            <div class="row clearfix">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px"  >كود المنتج  </label>
                                    <input  type="text" name="code4" id="code4" value="{{old('code4')}}"  class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px" >سعر القطعة الواحدة عند البيع </label>
                                    <input value="{{old('price4')}}" type="number" name="price4"    id="price4" class="form-control" >
                                    </div>
                                </div>
                                 <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px" >العدد </label>
                                    <input type="number" name="count4"  id="count4" value="{{old('count4')}}"  class="form-control" placeholder="العدد">
                                    </div>
                                </div>
                                        <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px" >صورة المنتج </label>
                                        <input type="file" id="image4" name="image4"    class="form-control" >
                                    </div>
                                </div>


                                <div class="col-sm-6">
                                    <div class="form-group mt-3">
                                    <label style="font-size: 20px" > اللون والمقاس   .... </label>

                                    <textarea rows="2" name="note4" id="note4"  class="form-control no-resize" placeholder=" اللون والمقاس  ">{{old('note4')}}</textarea>
                                    </div>
                                </div>



                            </div>
                             <div class="col-sm-12">
                                    <button type="button" onclick="pr4()" id="btn4" class="btn btn-primary">اضافة منتج جديد</button>
                                </div>
                        </div>
                    </div>
                    <div  id="pr5" hidden>


                            <hr>
                        <div class="header">
                            <h2 style="font-size: 20px"> تفاصيل المنتج الخامس </h3>
                        </div>
                        <div class="body">

                            <div class="row clearfix">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px"  >كود المنتج  </label>
                                    <input  type="text" name="code5" id="code5" value="{{old('code5')}}"  class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px" >سعر القطعة الواحدة عند البيع </label>
                                    <input value="{{old('price5')}}" type="number" name="price5"    id="price5" class="form-control" >
                                    </div>
                                </div>
                                 <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px" >العدد </label>
                                    <input type="number" name="count5"  id="count5" value="{{old('count5')}}"  class="form-control" placeholder="العدد">
                                    </div>
                                </div>
                                        <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px" >صورة المنتج </label>
                                        <input type="file" name="image5" id="image5"    class="form-control" >
                                    </div>
                                </div>


                                <div class="col-sm-6">
                                    <div class="form-group mt-3">
                                    <label style="font-size: 20px" > اللون والمقاس   .... </label>

                                    <textarea rows="2" name="note5" id="note5" class="form-control no-resize" placeholder=" اللون والمقاس  ">{{old('note5')}}</textarea>
                                    </div>
                                </div>



                            </div>

                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                      <button type="submit" class="btn btn-primary">تأكيد الطلب</button>
                    </div>
                </div>
            </div>
        </form>

        </div>
    </div>
    @endsection
    @section('script')
        <script>

         var price2=   document.getElementById("price2").value;
         var note2=document.getElementById("note2").value;
         var code2=document.getElementById("code2").value;
         var count2=document.getElementById("count2").value;
         var image2=document.getElementById("image2").value;
         if(price2||note2||code2||count2||image2){
             pr1();
         }
          var price3=   document.getElementById("price3").value;
         var note3=document.getElementById("note3").value;
         var code3=document.getElementById("code3").value;
         var count3=document.getElementById("count3").value;
         var image3=document.getElementById("image3").value;
         if(price3||note3||code3||count3||image3){
             pr2();
         }
          var price4=   document.getElementById("price4").value;
         var note4=document.getElementById("note4").value;
         var code4=document.getElementById("code4").value;
         var count4=document.getElementById("count4").value;
         var image4=document.getElementById("image4").value;
         if(price4||note4||code4||count4||image4){
             pr3();
         }
          var price5=   document.getElementById("price5").value;
         var note5=document.getElementById("note5").value;
         var code5=document.getElementById("code5").value;
         var count5=document.getElementById("count5").value;
         var image5=document.getElementById("image5").value;
         if(price5||note5||code5||count5||image5){
             pr4();
         }
            function pr1(){
                document.getElementById("pr2").hidden=false;
                document.getElementById("btn1").hidden=true;
            }
              function pr2(){
                document.getElementById("pr3").hidden=false;
                document.getElementById("btn2").hidden=true;
            }
              function pr3(){
                document.getElementById("pr4").hidden=false;
                document.getElementById("btn3").hidden=true;
            }
              function pr4(){
                document.getElementById("pr5").hidden=false;
                document.getElementById("btn4").hidden=true;
            }


            function cal(){

            }
        </script>

    @endsection

{{-- @endsection
@section('script')

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script>
    var profit=0;
    var realprice=0;
    var count =1;
    var oneitem=1;
    var userprice=document.getElementById("userprice").value;
    var showprofit=document.getElementById("profit");

     function getcode(){
            axios.post('/cms/user/searchorder', {
                code: document.getElementById("code").value,
            })
            .then(function (response) {
                this.realprice=response.data.icon;
                this.userprice=document.getElementById("userprice").value;
                  show();


            })
            .catch(function (error) {
              this.realprice=0;
               this.userprice=0;
                document.getElementById("profit").innerHTML="المنتج غير متوفر";

                // show();

            });
        }

        function show(){
            var show=document.getElementById("allprice");
             oneitem=document.getElementById("price").value;
            count =document.getElementById("co").value;
            var all=oneitem*count;
            show.innerHTML=all;

            var allprofit=all-(realprice*count);
            profit=(allprofit*userprice)/100;
            console.log(userprice);

            if(realprice==oneitem||oneitem<realprice){
                 showprofit.innerHTML="ضع الأسعار المناسبة";

            }

            else{
            showprofit.innerHTML=profit;

            }

        }



</script>

@endsection --}}
