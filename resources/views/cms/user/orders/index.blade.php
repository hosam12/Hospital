@extends('cms.user.parent')
@section('title','الطلبات')

@section('content')

  <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('user.dashbord')}}" style="font-size: 20px">الرئيسية</a></li>
                            <li class="breadcrumb-item " style="font-size: 20px" aria-current="page">الطلبات</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2 style="font-size: 20px">الطلبات</h2>

                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th style="font-size: 17px">اسم الزبون</th>
                                        <th style="font-size: 17px">الإمارة</th>
                                        <th style="font-size: 17px">العنوان</th>
                                        <th style="font-size: 17px">رقم الهاتف</th>
                                        <th style="font-size: 17px">رقم الواتس</th>
                                        <th style="font-size: 17px">مجموع الطلبية</th>
                                        <th style="font-size: 17px">حالة التسليم</th>
                                        <th style="font-size: 17px">تاريخ الإنشاء</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <span hidden>{{$i=0}}</span>
                                    @foreach ($orders as $order)
                                <span hidden>{{$i++}}</span>

                                    <tr>
                                    <td>{{$i}}</td>

                                    <td><span style="font-size: 17px">{{$order->name}}</span></td>
                                    <td><span style="font-size: 17px">{{$order->emara}}</span></td>
                                     <td><span style="font-size: 17px">{{$order->address}}</span></td>

                                    <td><span style="font-size: 17px">{{$order->mobile}}</span></td>
                                    <td><span style="font-size: 17px">{{$order->whats}}</span></td>


                                                    <td>
                                            <a class="btn btn-info btn-sm"
                                               href="{{route('order.show',[$order->id])}}">
                                                <i class="icon-note">
                                                </i>
                                                ({{$order->details_count}}) عدد المنتجات
                                            </a>
                                            <span class="badge badge-dark"></span>
                                        </td>

                                     <td>
                                         @if($order->status=='ublode'||$order->status=='waiting')
                                        <span style="font-size: 17px" class="badge badge-light">قيد الإعتماد</span>

                                     @elseif($order->status=='cancel')
                                    <span style="font-size: 17px" class="badge badge-danger">مرفوضة</span>
                                    @else
                                        <span style="font-size: 17px" class="badge badge-success">تم الرد</span>

                                        @endif

                                    </td>
                                    <td><span style="font-size: 17px">{{$order->created_at}}</span></td>





                                        {{-- <td>
                                            <a href="javascript:void(0);" class="btn btn-info btn-round">Interview</a>

                                        </td> --}}
                                    </tr>
                                         @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
