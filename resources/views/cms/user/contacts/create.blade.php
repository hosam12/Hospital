@extends('cms.user.parent')
@section('title','التواصل')
@section('content')
 <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h2>Inbox</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('user.dashbord')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('user.profile')}}">الصفحة الشخصية</a></li>
                            <li class="breadcrumb-item active" style="font-size: 20px" aria-current="page"> تواصُل مع المسؤولين</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="mail-inbox">
                            <div class="mobile-left">
                                <a href="javascript:void(0);" class="btn btn-primary toggle-email-nav"><i class="fa fa-bars"></i></a>
                            </div>

                            <div class="body mail-right check-all-parent">
                                <div class="mail-compose">
                                <form action="{{route('contact.store')}}" method="POST" enctype="multipart/form-data">
                                       @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </div>
                        @endif
                        @if (session()->has('message'))
                                        <div class="alert {{session()->get('status')}} alert-dismissible fade show"
                                             role="alert">
                                            <span> {{ session()->get('message') }}</span>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                         @endif
                                    @csrf

                                    {{-- <input type="text" cols="40" rows="20"  class="form-control"> --}}
                                    {{-- <textarea name="" id="" cols="30" rows="10"></textarea> --}}
                                     <div class="form-group">
                                            <label style="font-size: 20px" >العنوان</label>
                                            <input type="text" class="form-control" value="{{old('subject')}}" style="font-size: 20px" name="subject" placeholder="الرجاء ادخال العنوان">
                                        </div>
                                        <div class="form-group">
                                            <label style="font-size: 20px" >الموضوع</label>

                                       <textarea name="articale" class="form-control" id="" cols="30" rows="10">{{old('articale')}}</textarea>
                                        </div>
                                         <div class="row clearfix">
                                            <div class="col-lg-12">
                                                <input type="file"  width="590px" height="390px" class="dropify" name="image">
                                            </div>

                                        </div>

                                    <div class="m-t-30 text-right">
                                        <button type="submit" class="btn btn-success btn-round">إرسال</button>

                                         </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
