@extends('cms.user.parent')
@section('title','كافة الرسائل')
@section('content')

<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>الرسائل  </h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a  style="font-size: 20px" href="{{route('user.dashbord')}}">الصفحة الرئيسية</a></li>
                            <li class="breadcrumb-item active" style="font-size: 20px" aria-current="page">الرسائل</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>

            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-hover table-custom spacing5">
                            <thead>
                                <tr>
                                    {{-- <th>#</th> --}}
                                    <th>#</th>
                                    <th>عرض</th>
                                    <th>الموضوع</th>
                                    <th>الحالة</th>
                                     <th>المرسل</th>
                                    <th>وقت الإرسال</th>


                                </tr>
                            </thead>
                            <tbody>
                            <span hidden>{{$i=0}}</span>
                                @foreach ($contacts as $item)
                            <span hidden>{{++$i}}</span>
                                <tr>
                                    {{-- <td class="w60">
                                        <img src="../assets/images/xs/avatar1.jpg" data-toggle="tooltip" data-placement="top" title="" alt="Avatar" class="w35 rounded" data-original-title="Avatar Name">
                                    </td> --}}
                                <td><span class="list-name">{{$i}}</span></td>
                                  <td>  <a href="{{route('contact.show',[$item->id])}}"><span style="font-size: 17px" class="badge badge-warning"><i class="icon-user"></i> عرض</span></a>   </td>
                                    <td>{{$item->subject}}</td>
                                    <td>
                                        @if($item->replay)

                                        <span class="badge badge-success">تم الرد</span>
                                        @else
                                        <span class="badge badge-danger">لم يتم الرد</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($item->tybesender=='user')
                                    <span class="badge badge-info">{{Auth::user()->name}}</span>
                                        @else
                                    <span class="badge badge-info">Techno Band</span>
                                        @endif
                                    </td>
                                <td>{{$item->created_at->diffForHumans()}}</td>
                                </tr>
                                @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
