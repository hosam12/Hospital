@extends('cms.user.parent')
@section('title','الفئات')

@section('content')
     <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>Departments</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('user.dashbord')}}">الرئيسية</a></li>
                             <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('order.index')}}">الطلبات</a></li>
                            <li class="breadcrumb-item active" style="font-size: 20px" aria-current="page">الفئات</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>

            <div class="row clearfix">
                @foreach ($cate as $item)
                     <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                         {{-- <a href=""> --}}



                    <div class="card">
                    <a href="{{route('category.product',[$item->id])}}">


                        <img class="img-fluid rounded" style="height: 350px" src="{{url('images/categorys/'.$item->image)}}" alt="">
                         </a>
                        <div class="body">
                            <h6 class="mt-3">{{$item->name}}</h6>
                            <div class="text-muted">{{$item->desc}}</div>
                        </div>
                        <div class="card-footer">
                            <ul class="list-unstyled sm team-info margin-0">
                                <li class="mr-3"><small>المنتجات</small></li>
                                @foreach ($item->product as $pp)
                                    <li><img src="{{url('images/products/'.$pp->images[0]->image)}}" alt="Avatar"></li>

                                @endforeach

                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach


            </div>
        </div>
    </div>



@endsection
