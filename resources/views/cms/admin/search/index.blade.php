@extends('cms.admin.parent')
@section('title','عمليات البحث')

@section('content')


    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin.dashbord')}}" style="font-size: 20px">الرئيسية</a></li>
                            <li class="breadcrumb-item " style="font-size: 20px" aria-current="page">المسوقين</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{route('user.create')}}" class="btn btn-sm btn-primary" title="">مسوق جديد</a>
                        {{-- <a href="https://themeforest.net/item/oculux-bootstrap-4x-admin-dashboard-clean-modern-ui-kit/23091507" class="btn btn-sm btn-success" title="Themeforest"><i class="icon-basket"></i> Buy Now</a> --}}
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2 style="font-size: 20px">العدد</h2>

                        </div>
                        @if(count($users)>0)
                        <hr>
                        @endif
                        <span style="font-size: 17px" class="badge badge-success">المسوقين({{count($users)}})</span>
                        @if(count($users)>0)

                        <hr>
                        @endif
                        <div class="table-responsive">
                            {{-- <span style="font-size: 17px" class="badge badge-success">المسوقين({{count($users)}})</span> --}}
                            <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                                <thead>
                                @if(count($users)>0)
                                    <tr>
                                        <th>#</th>
                                        <th style="font-size: 17px">مشاهدة</th>
                                        <th style="font-size: 17px">الصورة</th>
                                        <th style="font-size: 17px"> رقم الهوية</th>
                                        <th style="font-size: 17px">الإسم</th>
                                        <th style="font-size: 17px">الإيميل</th>
                                        <th style="font-size: 17px">الطلبات</th>
                                        <th style="font-size: 17px">نسبة المسوق</th>
                                        <th style="font-size: 17px">العمر</th>
                                        <th style="font-size: 17px">العنوان</th>
                                        <th style="font-size: 17px">الهاتف</th>
                                        <th style="font-size: 17px">الواتسآب</th>
                                        <th style="font-size: 17px">الحالة الإجتماعية</th>
                                        <th style="font-size: 17px">حالة الحساب</th>
                                        <th style="font-size: 17px">الانشاء بواسطة</th>
                                        <th style="font-size: 17px"> تاريخ الإنشاء</th>

                                        <th style="font-size: 17px">الإعدادات</th>
                                    </tr>
                                    @endif
                                </thead>
                                <tbody>
                                    <span hidden>{{$i=0}}</span>
                                    @foreach ($users as $user)
                                <span hidden>{{$i++}}</span>

                                    <tr>
                                    <td>{{$i}}</td>
                                    <td>
                                    <a href="{{route('user.show',[$user->id])}}"><span style="font-size: 17px" class="badge badge-warning"><i class="icon-user"></i> عرض</span></a>   </td>
                                        <td class="w60">
                                            <img src="{{url('images/users/'.$user->image)}}" data-toggle="tooltip" data-placement="top" title="Avatar Name" alt="Avatar" class="w35 h35 rounded">
                                        </td>
                                        <td>
                                            <div class="font-15">{{$user->numberid}}</div>
                                        </td>
                                    <td><span style="font-size: 17px">{{$user->name}}</span></td>
                                    <td><span style="font-size: 17px">{{$user->email}}</span></td>
                                       <td>
                                            <a class="btn btn-info btn-sm"
                                               href="{{route('user.details',[$user->id])}}">
                                                <i class="icon-note">
                                                </i>
                                                ({{$user->details_count}}) عدد الطلبات
                                            </a>
                                            <span class="badge badge-dark"></span>
                                        </td>
                                     <td><span style="font-size: 17px">{{$user->userprice}} %</span></td>

                                    <td><span style="font-size: 17px">{{$user->age}}</span></td>
                                    <td><span style="font-size: 17px">{{$user->adress}}</span></td>
                                    <td><span style="font-size: 17px">{{$user->mobile}}</span></td>
                                    <td><span style="font-size: 17px">{{$user->now}}</span></td>
                                    <td><span style="font-size: 17px">{{$user->gender}}</span></td>
                                     <td>
                                         @if($user->status=='Active')
                                        <span style="font-size: 17px" class="badge badge-success">نشط</span>
                                        @else
                                    <span style="font-size: 17px" class="badge badge-danger">متوقف</span>
                                        @endif

                                    </td>
                                    @if($user->admin_id)
                                    <td><span>{{$user->admin->name}}</span></td>
                                    @else
                                    <td><span>التسجيل عبرالموقع</span></td>
                                    @endif
                                    <td><span>{{$user->created_at}}</span></td>

                                        <td>
                                        <a href="{{route('user.edit',[$user->id])}}" type="button" style="font-size: 20px" class="btn btn-sm btn-default" title="تعديل"><i class="fa fa-edit"></i> تعديل</a>
                                        {{-- <a onclick="confirmDelete(this, '{{$user->id}}')" type="button" style="font-size: 20px" class="btn btn-sm btn-default js-sweetalert" title="حذف" data-type="confirm"><i class="fa fa-trash-o text-danger"> حذف </i></a> --}}
                                        {{-- <button type="button" class="btn btn-sm btn-default" title="عرض"><i class="icon-diamond"></i></button> --}}

                                        </td>




                                        {{-- <td>
                                            <a href="javascript:void(0);" class="btn btn-info btn-round">Interview</a>

                                        </td> --}}
                                    </tr>
                                         @endforeach
                                </tbody>
                            </table>
                            </div>
                            @if(count($products)>0)
                            <hr>
                            @endif
                         <span style="font-size: 17px" class="badge badge-success">المنتجات({{count($products)}})</span>

                            <hr>

                            {{-- //////////////////////////// --}}
                            {{-- المنتجات --}}
                         {{-- <span style="font-size: 17px" class="badge badge-success">المنتجات({{count($products)}})</span> --}}
                                                    <div class="table-responsive">

                              <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                                <thead>
                                     @if(count($products)>0)

                                    <tr>

                                        <th>#</th>
                                        <th style="font-size: 17px">مشاهدة</th>
                                        {{-- <th style="font-size: 17px">الصورة</th> --}}
                                        <th style="font-size: 17px">اسم المنتج</th>
                                        <th style="font-size: 17px"># الكود</th>
                                        <th style="font-size: 17px">الفئة</th>
                                        <th style="font-size: 17px">سعر الجملة</th>
                                        <th style="font-size: 17px">الحد الأدنى</th>
                                        <th style="font-size: 17px">الحد الأقصى</th>
                                        <th style="font-size: 17px"> السعر الثابت</th>
                                        <th style="font-size: 17px">جنس المنتج</th>
                                        <th style="font-size: 17px">الألوان المتوفرة</th>
                                        <th style="font-size: 17px">حالة المنتج</th>
                                        <th style="font-size: 17px"> تاريخ الإنشاء</th>
                                        <th style="font-size: 17px">الإعدادات</th>
                                    </tr>
                                @endif
                                </thead>
                                <tbody>
                                    <span hidden>{{$i=0}}</span>
                                    @foreach ($products as $product)
                                <span hidden>{{$i++}}</span>

                                    <tr>
                                    <td>{{$i}}</td>
                                    <td>
                                    <a href="{{route('product.edit',[$product->id])}}"><span style="font-size: 17px" class="badge badge-warning"><i class="icon-user"></i> عرض</span></a>
                                 </td>
                                        {{-- <td class="w60">
                                            <img src="{{url('images/users/'.$user->image)}}" data-toggle="tooltip" data-placement="top" title="Avatar Name" alt="Avatar" class="w35 h35 rounded">
                                        </td> --}}
                                        <td>
                                            <div class="font-15">{{$product->name}}</div>
                                        </td>
                                    <td><span style="font-size: 17px" >{{$product->code}}</span></td>
                                    <td><span style="font-size: 17px">{{$product->category->name}}</span></td>
                                     <td><span style="font-size: 17px">{{$product->realprice}}</span></td>

                                    <td>
                                        @if($product->from)
                                         <span  class="badge badge-success" style="font-size: 17px">{{$product->from}}</span>
                                         @else
                                        <span  class="badge badge-danger" style="font-size: 17px">السعر ثابت</span>

                                        @endif



                                    </td>
                                    <td>
                                                         @if($product->to)
                                         <span  class="badge badge-success" style="font-size: 17px">{{$product->to}}</span>
                                         @else
                                        <span  class="badge badge-danger" style="font-size: 17px">السعر ثابت</span>

                                        @endif
                                    </td>
                                    <td><span style="font-size: 17px">

                                                         @if($product->staticprice)
                                         <span  class="badge badge-success" style="font-size: 17px">{{$product->staticprice}}</span>
                                         @else
                                        <span  class="badge badge-danger" style="font-size: 17px">السعر مُتغير</span>

                                        @endif
                                    </span></td>
                                    <td><span style="font-size: 17px">

                                       @if ($product->gender=='Male')
                                           <span style="font-size: 17px"> ذكور</span>


                                           @elseif($product->gender=='Female')
                                               <span style="font-size: 17px"> إناث</span>
                                          @else
                                             <span style="font-size: 17px"> ذكور وإناث</span>

                                       @endif


                                    </span></td>
                                    <td>
                                        @if($product->color)
                                        <span style="font-size: 17px">{{ Str::limit($product->color,30)}}</span>
                                        @else
                                        <span style="font-size: 17px" class="badge badge-warning">---</span>
                                        @endif

                                    </td>
                                     <td>
                                         @if($product->status=='Visible')
                                        <span style="font-size: 17px" class="badge badge-success">فعّال</span>
                                        @else
                                    <span style="font-size: 17px" class="badge badge-danger">متوقف</span>
                                        @endif

                                    </td>

                                    <td><span>{{$product->created_at->diffForHumans()}}</span></td>

                                        <td>
                                        <a href="{{route('product.edit',[$product->id])}}" type="button" style="font-size: 20px" class="btn btn-sm btn-default" title="تعديل"><i class="fa fa-edit"></i> تعديل</a>
                                        {{-- <button type="button" class="btn btn-sm btn-default" title="عرض"><i class="icon-diamond"></i></button> --}}

                                        </td>




                                        {{-- <td>
                                            <a href="javascript:void(0);" class="btn btn-info btn-round">Interview</a>

                                        </td> --}}
                                    </tr>
                                         @endforeach
                                </tbody>
                            </table>
                            </div>
                            <hr>
                                <span style="font-size: 17px" class="badge badge-success">الفئات({{count($categorys)}})</span>
                            @if(count($categorys)>0)
                                <hr>
                                @endif

                                     <div class="table-responsive">

                             <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                                <thead>
                            @if(count($categorys)>0)

                                    <tr>
                                        <th >#</th>
                                          <th style="font-size: 17px">صورة الفئة</th>
                                        <th style="font-size: 17px">الفئة</th>
                                        <th style="font-size: 17px">منتجات الفئة</th>
                                        <th style="font-size: 17px">حالة الفئة</th>

                                        {{-- <th style="font-size: 17px">منتجات الفئة</th> --}}
                                        <th style="font-size: 17px">الإعدادات</th>
                                    </tr>
                                    @endif
                                </thead>
                                <tbody>
                                    <span hidden>{{$i=0}}</span>
                                    @foreach ($categorys as $category)
                                <span hidden>{{$i++}}</span>

                                    <tr>
                                    <td>{{$i}}</td>
                                        <td class="w60">
                                            <img src="{{url('images/categorys/'.$category->image)}}" data-toggle="tooltip" data-placement="top" title="Avatar Name" alt="Avatar" class="w35 h35 rounded">
                                        </td>

                                    <td><span style="font-size: 17px">{{$category->name}}</span></td>
                                      <td>
                                            <a class="btn btn-info btn-sm"
                                               href="{{route('category.product',[$category->id])}}">
                                                <i class="icon-note">
                                                </i>
                                                ({{$category->product_count}}) عدد المنتجات
                                            </a>
                                            <span class="badge badge-dark"></span>
                                        </td>
                                     <td>
                                         @if($category->status=='Visible')
                                        <span style="font-size: 17px" class="badge badge-success">نشطة</span>
                                        @else
                                    <span style="font-size: 17px" class="badge badge-danger">متوقفة</span>
                                        @endif

                                    </td>
                                        <td>
                            <a href="{{route('category.edit',[$category->id])}}" type="button" style="font-size: 20px" class="btn btn-sm btn-default" title="تعديل"><i class="fa fa-edit"></i> تعديل</a>
                                        <a onclick="confirmDelete(this, '{{$category->id}}')" type="button" style="font-size: 20px" class="btn btn-sm btn-default js-sweetalert" title="حذف" data-type="confirm"><i class="fa fa-trash-o text-danger"> حذف </i></a>


                                        </td>




                                        {{-- <td>
                                            <a href="javascript:void(0);" class="btn btn-info btn-round">Interview</a>

                                        </td> --}}
                                    </tr>
                                         @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
