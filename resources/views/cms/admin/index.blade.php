@extends('cms.admin.parent')
@section('title','Home')

@section('content')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>Dashboard</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a style="font-size: 20px" href="#">الطلبات</a></li>
                            <li class="breadcrumb-item"><a style="font-size: 20px" href="#">المسوقين</a></li>
                            <li class="breadcrumb-item active" style="font-size: 20px" aria-current="page">الرئيسة</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{route('user.create')}}" class="btn btn-sm btn-primary" style="font-size: 20px" title="">مسوق جديد</a>
                        {{-- <a href="https://themeforest.net/item/oculux-bootstrap-4x-admin-dashboard-clean-modern-ui-kit/23091507" class="btn btn-sm btn-success" title="Themeforest"><i class="icon-basket"></i> Buy Now</a> --}}
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card w_card3">
                        <div class="body">
                            <div class="text-center"><i class=" icon-hourglass"> New</i>
                            <h5 class="m-t-20 mb-0">{{$orders->where('status','ublode')->count()}}</h5>
                                <p class="text-muted">طلبات جديدة </p>
                                <a href="{{route('order.ublode')}}" class="btn btn-info btn-round">ذهاب </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card w_card3">
                        <div class="body">
                            <div class="text-center"><i class="icon-calendar "></i>
                                <h5 class="m-t-20 mb-0">{{$month->count()}}</h5>
                                <p class="text-muted">الطلبات الشهرية</p>
                                <a href="{{route('details.month')}}" class="btn btn-info btn-round">ذهاب</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card w_card3">
                        <div class="body">
                            <div class="text-center  "><i class="icon-mouse"></i>
                                <h5 class="m-t-20 mb-0">{{$details->count()}}</h5>
                                <p class="text-muted">إجمالي عدد الطلبيات</p>
                                <a href="{{route('details.index')}}" class="btn btn-info btn-round">ذهاب</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card w_card3">
                        <div class="body">
                            <div class="text-center"><i class=" icon-user"></i>
                                <h5 class="m-t-20 mb-0">{{$users->count()}}</h5>
                                <p class="text-muted">إجمالي المسوقين</p>
                                <a href="{{route('user.index')}}" class="btn btn-info btn-round">ذهاب</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <div class="row clearfix">
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card w_card3">
                        <div class="body">
                            <div class="text-center"><i class="  icon-check"></i>
                                <h5 class="m-t-20 mb-0">{{$details->where('status','success')->count()}}</h5>
                                <p class="text-muted">الطلبات الناجحة </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card w_card3">
                        <div class="body">
                            <div class="text-center"><i class=" icon-grid"></i>
                                <h5 class="m-t-20 mb-0">{{$details->where('status','waiting')->count()}}</h5>
                                <p class="text-muted"> طلبات  قيد الإعتماد</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card w_card3">
                        <div class="body">
                            <div class="text-center  "><i class=" icon-graph"></i>
                            <h5 class="m-t-20 mb-0">{{$month->where('status','success')->sum('total')}}</h5>
                                <p class="text-muted">إجمالي البيع لهذ الشهر</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card w_card3">
                        <div class="body">
                            <div class="text-center"><i class="  icon-calculator"></i>
                                <h5 class="m-t-20 mb-0">{{$details->where('status','success')->sum('total')}}</h5>
                                <p class="text-muted">إجمالي البيع</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                @foreach ($admins as $item)

                <div class="col-lg-3 col-md-12">
                    <div class="card c_grid c_yellow">
                        <div class="body">
                            <div class="circle w60 ">
                                <img class="rounded-circle" width="128px" height="90px" src="{{url('images/admins/'.$item->image)}}" alt="">
                            </div>
                            <div class="text-center mb-5">
                            <h6 class="mt-3 mb-0">{{$item->name}}</h6>
                                <span>{{$item->email}}</span>
                                <span>{{$item->now}}</span>

                                <ul class="mt-3 list-unstyled d-flex justify-content-center">
                                    <li><a class="p-3" target="_blank" href="{{$item->urlfase}}"><i class="fa fa-facebook"></i></a></li>
                                    <li><a class="p-3" target="_blank" href="{{$item->urlwats}}"><i class="fa fa-whatsapp"></i></a></li>
                                    <li><a class="p-3" target="_blank" href="{{$item->urlinsta}}"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
                @endforeach


            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2>الطلبات الجديدة</h2>

                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>اسم المسوق</th>
                                        <th>تاريخ الإنشاء</th>
                                        <th>الحالة</th>
                                        <th>العرض</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($orders->where('status','ublode') as $order)
                                         <tr>
                                       <td class="w60">
                                            <img src="{{url('images/users/'.$order->user->image)}}" data-toggle="tooltip" data-placement="top" title="{{$order->name}}"  class="w35 h35 rounded">
                                                </td>
                                        <td>
                                        <div class="font-15">{{$order->user->name}}</div>
                                        </td>
                                    <td> {{$order->created_at}}</td>
                                        <td><span style="font-size: 17px" class="badge badge-light">لم يتم تسليمها</span></td>
                                        <td>
                                        <a href="{{route('order.show',[$order->id])}}" class="btn btn-info btn-round">عرض</a>
                                        </td>
                                    </tr>
                                    @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
