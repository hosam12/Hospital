@extends('cms.admin.parent')
@section('title','تعديل ميزة')

@section('content')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1 style="font-size: 20px"> تعديل ميزة</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin.dashbord')}}" style="font-size: 20px">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="{{route('ser.index')}}" style="font-size: 20px">الميزات</a></li>
                            <li class="breadcrumb-item"><a href="{{route('ser.create')}}" style="font-size: 20px">ميزة جديدة </a></li>

                            <li class="breadcrumb-item active" aria-current="page"style="font-size: 20px"> تعديل ميزة</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                       {{-- <a href="https://themeforest.net/item/oculux-bootstrap-4x-admin-dashboard-clean-modern-ui-kit/23091507" class="btn btn-sm btn-success" title="Themeforest"><i class="icon-basket"></i> Buy Now</a> --}}
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="body">
                        <form method="POST" action="{{route('ser.update',[$serve->id])}}" enctype="multipart/form-data">


                            @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </div>
                        @endif
                        @if (session()->has('message'))
                                        <div class="alert {{session()->get('status')}} alert-dismissible fade show"
                                             role="alert">
                                            <span> {{ session()->get('message') }}</span>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                            @csrf
                            @method("PUT")
                            <div  style="align-content: center" class="col-lg-2 col-md-12 center text-center">
                        <div class="body text-center">
                        <img class="img-thumbnail rounded-circle" src="{{url('images/serves/'.$serve->image)}}" alt="{{$serve->name}}">
                            <h6 class="mt-3">{{$serve->name}}</h6>
                        </div>
                </div>
                                <div class="form-group row">
                                    <div class="col-md-10 col-sm-10">



                                        <div class="form-group">
                                            <label style="font-size: 20px" >الإسم</label>
                                        <input type="text" class="form-control" value="{{$serve->name}}" style="font-size: 20px" name="name" placeholder=" ادخل الإسم رباعي ">
                                        </div>




                                            <label style="font-size: 20px" >  الميزة</label>

                                         <div class="row clearfix">
                                            <div class="col-lg-12">
                                                <input type="file" width="590px" height="390px" class="dropify" name="image">
                                            </div>

                                        </div>



                            <div class="form-group">
                                <label style="font-size: 20px">حالة عرض الميزة</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="status"
                                        id="status"
                                        @if($serve->status == 'Active') checked @endif>
                                    <label class="custom-control-label" style="font-size: 20px" for="status">مُفعل</label>

                        </div>
                    </div>

                                    </div>
                                </div>
                                <div>
                                            <button style="font-size: 20px" class="btn btn-sm btn-primary" type="submit"> تعديل  </button>


                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
