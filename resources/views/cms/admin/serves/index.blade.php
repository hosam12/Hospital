@extends('cms.admin.parent')
@section('title',' الميزات')

@section('content')
    <div id="main-content">
        <div class="container">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h2>الميزات</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('ser.all')}}">تعديل الميزات</a></li>
                            <li class="breadcrumb-item"><a  style="font-size: 20px" href="{{route('admin.show',[Auth::user()->id])}}">الرئيسية</a></li>
                            <li class="breadcrumb-item active" style="font-size: 20px" aria-current="page">صور </li>
                            </ol>
                        </nav>
                    </div>
                            <div class="col-md-6 col-sm-12 text-right hidden-xs">
                            <a href="{{route('ser.create')}}" class="btn btn-sm btn-primary btn-round" title=""><i class="fa fa-cloud-upload"></i> ميزة جديدة</a>
                        </div>
                </div>
            </div>

            <div id="lightgallery" class="row clearfix lightGallery">
                @foreach ($images as $item)
                     <div class="col-lg-3 col-md-6 m-b-30"><a class="light-link" href="{{url('images/serves/'.$item->image)}}"><img class="img-fluid rounded"  style="height: 380px"  src="{{url('images/serves/'.$item->image)}}" alt=""></a></div>
                @endforeach

            </div>
        </div>
    </div>
@endsection
@section('script')

<script src="{{asset('cms/html/assets/js/pages/medias/image-gallery.js')}}"></script>
<script src="{{asset('cms/html/assets/bundles/lightgallery.bundle.js')}}"></script><!-- Light Gallery Plugin Js -->

@endsection
