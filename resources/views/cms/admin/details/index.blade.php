@extends('cms.admin.parent')
@section('title','تفاصيل الطلبية')
@section('content')
       <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>كافة الطلبات</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('admin.dashbord')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a  style="font-size: 20px" href="{{route('order.index')}}">الطلبيات</a></li>
                            <li class="breadcrumb-item active"  style="font-size: 20px" aria-current="page">تفاصيل الطلبيات</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>

            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-hover table-custom spacing5">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>المسوق</th>
                                    <th>اسم المسوق</th>
                                    <th>رقم الطلبية</th>
                                    <th>الزبون</th>
                                    <th>عنوان الزبون</th>
                                    <th>الإمارة</th>
                                    <th>المنتج</th>
                                    <th>البيع للقطعة الواحدة</th>
                                    <th>العدد</th>
                                    <th>إجمالي البيع</th>
                                    <th>نسبة المسوق</th>
                                    <th>الربح</th>
                                    <th>الحالة</th>
                                    <th>تاريخ الإنشاء</th>

                                </tr>
                            </thead>
                            <tbody>
                                      <span hidden>{{$i=0}}</span>
                                @foreach ($details as $item)
                                <span hidden>{{$i++}}</span>

                                <tr>
                                <td>{{$i}}</td>
                                    <td class="w60">
                                        {{-- <a href="">

                                        </a> --}}
                                    <a href="{{route('details.edit',[$item->id])}}">
                                    <img src="{{url('images/users/'.$item->order->user->image)}}" data-toggle="tooltip" data-placement="top" title="{{$item->order->user->name}}" alt="{{$item->order->user->name}}" class="w35 rounded" data-original-title="Avatar Name">

                                        </a>
                                    </td>
                                <td><span class="list-name">{{$item->order->user->name}}</span></td>
                                 <td>{{$item->order->id}}</td>
                                    <td>{{$item->order->name}}</td>
                                    <td>{{$item->order->address}}</td>
                                    <td>{{$item->order->emara}}</td>
                                <td>{{$item->product->name}}</td>
                                <td>{{$item->currentPrice}}</td>
                                    <td>{{$item->count}}</td>
                                    <td>{{$item->total}}</td>
                                    <td>{{$item->currentuserprice}} %</td>

                                <td>
                                    @if ($item->status=='success')
                                    {{$item->profit}}
                                    @else
                                    {{0}}
                                    @endif

                                </td>
                                    <td>
                                        @if ($item->status=='success')
                                        <span class="badge badge-success">نجاح</span>
                                        @elseif($item->status=='waiting')
                                         <span class="badge badge-warning">قيد الإعتماد</span>
                                    @else
                                        <span class="badge badge-danger">مرفوضة</span>

                                        @endif
                                    </td>
                                      <td>{{$item->created_at}}</td>

                                </tr>
                                    @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
