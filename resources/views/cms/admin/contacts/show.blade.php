@extends('cms.admin.parent')
@section('title','الرسالة')
@section('content')
 <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h2>الرسالة</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('admin.dashbord')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('admin.show',[Auth::user()->id])}}">الصفحة الشخصية</a></li>
                            <li class="breadcrumb-item active"  style="font-size: 20px" aria-current="page">الرسائل</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>
        </div>
        <div class="container">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="single-blog">

                        <div class="post-content overflow">
                        <h1 class="post-title " ><a href="#">الموضوع</a></h1>
                        <h2 class="post-title bold"><a href="#">{{$contact->subject}}</a></h2>
                        <p style="font-size: 17px">{{$contact->articale}}</p>

                        </div>

                        <div class="post-thumb">
                            @if($contact->image)
                        <a href="#"><img src="{{url('images/contacts/'.$contact->image)}}" class="img-fluid rounded" alt="Techno Band"></a>
                       @endif
                    </div>

                    </div>
                    <hr>

                    @if($contact->replay&&$contact->tybesender=='user')
                 <span class="badge badge-success">تم الرد</span>
                            <div class="post-content overflow">
                        <p style="font-size: 17px">{{$contact->replay}}</p>
                        </div>
                        @else
                    <span class="badge badge-danger">لم يتم الرد</span>

                        @endif

                        <div class="post-content overflow">
                            @if($contact->tybesender=='user')
                        <h2 style="color: black" ><a href="#">المرسل</a></h2>
                        <small>{{$contact->created_at}}</small>
                        <h1 class="post-title " ><a href="#">{{$contact->user->name}}</a></h1>

                        @else
                        <h2  style="color: black" ><a href="#">المستقبل</a></h2>
                        <small>{{$contact->created_at}}</small>
                        <h1  class="post-title bold"><a href="#">{{$contact->user->name}}  </a></h1>
                            @endif

                        </div>
                        {{-- <small>تم الإنشاء</small><br> --}}
                    {{-- <small>{{$contact->creates_at}}</small> --}}
                                <hr>
                                @if($contact->replay)
                                <h1>تم الرد</h1>
                                <p>{{$contact->replay}}</p>

                                            @else

                    <form action="{{route('contact.update',[$contact->id])}}" method="POST">
                        @csrf
                        @method("PUT")



                                             <div class="form-group">
                                            <label style="font-size: 20px" >اضف ردك</label>
                                       <textarea name="replay" class="form-control" id="" cols="30" rows="10">{{old('replay')}}</textarea>
                                        </div>

                                    <div class="m-t-30 text-right">
                                        <button type="submit" class="btn btn-success btn-round">إرسال</button>

                                         </form>


                                        @endif
                </div>
            </div>
        </div>
    </div>
@endsection

