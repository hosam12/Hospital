@extends('cms.admin.parent')
@section('title','كافة الرسائل')
@section('content')

<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>الرسائل  </h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a  style="font-size: 20px" href="{{route('admin.dashbord')}}">الصفحة الرئيسية</a></li>
                            <li class="breadcrumb-item active" style="font-size: 20px" aria-current="page">الرسائل</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>

            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-hover table-custom spacing5">
                            <thead>
                                <tr>
                                    {{-- <th>#</th> --}}
                                    <th>#</th>
                                    <th>عرض</th>
                                    <th>الموضوع</th>
                                    <th>الحالة</th>
                                     <th>المرسل</th>
                                    <th>وقت الإرسال</th>
                                    <th>الحذف</th>


                                </tr>
                            </thead>
                            <tbody>
                            <span hidden>{{$i=0}}</span>
                                @foreach ($contacts as $item)
                            <span hidden>{{++$i}}</span>
                                <tr>
                                    {{-- <td class="w60">
                                        <img src="../assets/images/xs/avatar1.jpg" data-toggle="tooltip" data-placement="top" title="" alt="Avatar" class="w35 rounded" data-original-title="Avatar Name">
                                    </td> --}}
                                <td><span class="list-name">{{$i}}</span></td>
                                  <td>  <a href="{{route('contact.show',[$item->id])}}"><span style="font-size: 17px" class="badge badge-warning"><i class="icon-user"></i> عرض</span></a>   </td>
                                    <td>{{$item->subject}}</td>
                                    <td>
                                        @if($item->replay)

                                        <span class="badge badge-success">تم الرد</span>
                                        @else
                                        <span class="badge badge-danger">لم يتم الرد</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($item->tybesender=='admin')
                                    <span class="badge badge-info">admin</span>
                                        @else
                                    <span class="badge badge-info">{{$item->user->name}}</span>
                                        @endif
                                    </td>
                                <td>{{$item->created_at->diffForHumans()}}</td>
                                <td>
                                <a onclick="confirmDelete(this, '{{$item->id}}')" type="button" style="font-size: 20px" class="btn btn-sm btn-default js-sweetalert" title="حذف" data-type="confirm"><i class="fa fa-trash-o text-danger"> حذف </i></a>
                                </td>
                                </tr>
                                @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script>
        function confirmDelete(app, id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    deletecontact(app, id)
                }
            })
        }

        function deletecontact(app, id) {
            axios.delete('/contact/' + id)
                .then(function (response) {
                    // handle success (Status Code: 200)
                    console.log(response);
                    console.log(response.data);
                    showMessage(response.data);
                    app.closest('tr').remove();
                })
                .catch(function (error) {
                    // handle error (Status Code: 400)
                    console.log(error);
                    console.log(error.response.data);
                    showMessage(error.response.data);
                })
                .then(function () {
                    // always executed
                });
        }

        function showMessage(data) {
            Swal.fire({
                position: 'center',
                icon: data.icon,
                title: data.title,
                showConfirmButton: false,
                timer: 1500
            })
        }
    </script>

@endsection
