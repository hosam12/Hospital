@extends('cms.admin.parent')
@section('title','الطلبات')

@section('content')

  <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin.dashbord')}}" style="font-size: 20px">الرئيسية</a></li>
                            <li class="breadcrumb-item " style="font-size: 20px" aria-current="page">الطلبات</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{route('user.create')}}" class="btn btn-sm btn-primary" title="">مسوق جديد</a>
                        {{-- <a href="https://themeforest.net/item/oculux-bootstrap-4x-admin-dashboard-clean-modern-ui-kit/23091507" class="btn btn-sm btn-success" title="Themeforest"><i class="icon-basket"></i> Buy Now</a> --}}
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2 style="font-size: 20px">الطلبات</h2>

                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th style="font-size: 17px">عرض</th>
                                        <th style="font-size: 17px">رقم الطلبية</th>
                                        <th style="font-size: 17px">اسم المسوق</th>
                                        <th style="font-size: 17px">اسم الزبون</th>
                                        <th style="font-size: 17px">الإمارة</th>
                                        <th style="font-size: 17px">العنوان</th>
                                        <th style="font-size: 17px">رقم الهاتف</th>
                                        <th style="font-size: 17px">رقم الواتس</th>
                                        <th style="font-size: 17px">مجموع الطلبية</th>
                                        <th style="font-size: 17px">حالة التسليم</th>
                                        <th style="font-size: 17px">تاريخ الإنشاء</th>
                                         <th style="font-size: 17px">عملية</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    <span hidden>{{$i=0}}</span>
                                    @foreach ($orders as $order)
                                <span hidden>{{$i++}}</span>

                                    <tr>
                                    <td>{{$i}}</td>
                                    <td>
                                    <a href="{{route('order.show',[$order->id])}}"><span style="font-size: 17px" class="badge badge-warning"><i class="icon-user"></i> عرض</span></a>   </td>

                                    <td style="font-size: 17px">{{$order->id}}</td>
                                        <td>
                                            <div class="font-15">{{$order->user->name}}</div>
                                        </td>
                                    <td><span style="font-size: 17px">{{$order->name}}</span></td>
                                    <td><span style="font-size: 17px">{{$order->emara}}</span></td>
                                     <td><span style="font-size: 17px">{{$order->address}}</span></td>

                                    <td><span style="font-size: 17px">{{$order->mobile}}</span></td>
                                    <td><span style="font-size: 17px">{{$order->whats}}</span></td>


  <td>
                                            <a class="btn btn-info btn-sm"
                                               href="{{route('order.show',[$order->id])}}">
                                                <i class="icon-note">
                                                </i>
                                                ({{$order->details_count}}) عدد المنتجات
                                            </a>
                                            <span class="badge badge-dark"></span>
                                        </td>

                                     <td>
                                         @if($order->status=='ublode')
                                        <span style="font-size: 17px" class="badge badge-light">لم يتم التسليم</span>
                                        @elseif($order->status=='waiting')
                                    <span style="font-size: 17px" class="badge badge-warning">قيد الإعتماد</span>
                                     @elseif($order->status=='cancel')
                                    <span style="font-size: 17px" class="badge badge-danger">مرفوضة</span>
                                    @else
                                        <span style="font-size: 17px" class="badge badge-success">تم الرد</span>

                                        @endif

                                    </td>
                                    <td><span style="font-size: 17px">{{$order->created_at}}</span></td>
                                    <td>
                                        <a onclick="confirmDelete(this, '{{$order->id}}')" type="button" style="font-size: 20px" class="btn btn-sm btn-default js-sweetalert" title="حذف" data-type="confirm"><i class="fa fa-trash-o text-danger"> حذف </i></a>


                                    </td>





                                        {{-- <td>
                                            <a href="javascript:void(0);" class="btn btn-info btn-round">Interview</a>

                                        </td> --}}
                                    </tr>
                                         @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script>
        function confirmDelete(app, id) {
            Swal.fire({
                title: 'هل أنت متأكد ؟',
                text: "هل انت متأكد من حذف الصورة ؟!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes Deleted!'
                // cancelButtonText: 'نعم!'

            }).then((result) => {
                if (result.value) {
                    deleteproduct(app, id)
                }
            })
        }

        function deleteproduct(app, id) {
            axios.delete('/order/' + id)
                .then(function (response) {
                    // handle success (Status Code: 200)
                    console.log(response);
                    console.log(response.data);
                    showMessage(response.data);
                    app.closest('tr').remove();
                })
                .catch(function (error) {
                    // handle error (Status Code: 400)
                    console.log(error);
                    console.log(error.response.data);
                    showMessage(error.response.data);
                })
                .then(function () {
                    // always executed
                });
        }

        function showMessage(data) {
            Swal.fire({
                position: 'center',
                icon: data.icon,
                title: data.title,
                showConfirmButton: false,
                timer: 1500
            })
        }
    </script>

@endsection

