@extends('cms.admin.parent')
@section('title','تعديل')
@section('content')

<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>تعديل </h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{route('order.show',[$details->order->id])}}" style="font-size: 20px">رجوع</a></li>
                            <li class="breadcrumb-item"><a  style="font-size: 20px" href="{{route('user.dashbord')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a   style="font-size: 20px" href="#">الطلبيات</a></li>
                            <li class="breadcrumb-item active" style="font-size: 20px" aria-current="page">طلبية جديدة</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2 style="font-size: 20px">بيانات الزبون </h3>
                        </div>
                        <form action="{{route('details.update',[$details->id])}}" method="POST" enctype="multipart/form-data">
                         @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </div>
                        @endif
                        @if (session()->has('message'))
                                        <div class="alert {{session()->get('status')}} alert-dismissible fade show"
                                             role="alert">
                                            <span> {{ session()->get('message') }}</span>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                         @endif
                        @csrf
                        @method("PUT")
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="form-group mb-3">
                                        <label style="font-size: 20px" >اسم الزبون</label>
                                    <input type="text" value="{{$details->order->name}}" name="name" disabled class="form-control" placeholder="اسم الزبون">
                                    </div>
                                </div>
                               <div class="col-md-6">
                                    <div class="form-group mb-3">
                                        <label style="font-size: 20px" >الإمارة</label>
                                    <input type="text" value="{{$details->order->emara}}" disabled name="emara" class="form-control" placeholder="ادخل  الإمارة">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group mb-3">
                                        <label style="font-size: 20px" >العنوان </label>
                                    <input type="text" value="{{$details->order->address}}" disabled name="address" disabled class="form-control" placeholder="أدخل العنوان ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group mb-3">
                                        <label style="font-size: 20px" > رقم الهاتف للتواصل</label>
                                    <input type="text" value="{{$details->order->mobile}}" name="mobile" disabled class="form-control" placeholder="رقم الهاتف">
                                    </div>
                                </div>
                                  <div class="col-md-6">
                                    <div class="form-group mb-3">
                                        <label style="font-size: 20px" >رقم الواتس</label>
                                    <input type="text" value="{{$details->order->whats}}" disabled name="whats" class="form-control" placeholder="أدخل رقم الواتس">
                                    </div>
                                </div>

                            </div>

                        </div>
                    <div>

                            <hr>
                        <div class="header">
                            <h2 style="font-size: 20px"> تفاصيل المنتج </h3>
                        </div>
                        <div class="body">

                            <div class="row clearfix">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px"  >كود المنتج  </label>
                                    <input  type="text" name="code" value="{{$details->product->code}}"  class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px" >سعر القطعة الواحدة عند البيع </label>
                                    <input value="{{$details->currentPrice}}" type="number" name="price"    id="price1" class="form-control" >
                                    </div>
                                </div>
                                 <div class="col-sm-3">
                                    <div class="form-group">
                                        <label style="font-size: 20px" >العدد </label>
                                    <input type="number" name="count"  id="count" value="{{$details->count}}"  class="form-control" placeholder="العدد">
                                    </div>
                                </div><br>
                       <div  style="align-content: center" class="col-lg-2 col-md-12 center text-center">
                        <div class="body text-center">
                        <img class="img-thumbnail rounded-circle" src="{{url('images/orders/'.$details->image)}}" alt="{{$details->product->name}}">
                        </div>
                </div>

                 <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                    <a href="{{route('daw.order',[$details->image])}}" class="btn btn-success"> تنزيل </a>
                    </div>
                </div>
            </div>

                     <div class="col-sm-12s">
                                    <div class="form-group">
                                        <label style="font-size: 20px" >صورة المنتج </label>
                                        <input type="file" name="image2" id="image2"   class="form-control" >
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group mt-3">
                                    <label style="font-size: 20px" >اللون والمقاس</label>

                                    <textarea rows="2" name="note"  class="form-control no-resize" placeholder=" اللون والمقاس ">{{$details->note}}</textarea>
                                    </div>
                                </div>
                                            <div class="form-group">
                                <label style="font-size: 20px">حالة التسليم</label>

                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="success"
                                        name="status" value="success"
                                        @if($details->status == 'success') checked @endif>
                                    <label style="font-size: 15px" for="success" class="custom-control-label">نجحت العملية</label>
                                </div>
                                  <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="cancel"
                                        name="status" value="cancel"
                                        @if($details->status == 'cancel') checked @endif>
                                    <label style="font-size: 15px" for="cancel" class="custom-control-label">مرفوضة</label>
                                </div>
                            </div>



                            </div>

                        </div>
                    </div>

                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                      <button type="submit" class="btn btn-primary">تعديل</button>
                    </div>
                </div>
            </div>
        </form>

        </div>
    </div>
@endsection
