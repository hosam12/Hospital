@extends('cms.admin.parent')
@section('title','ِAdmin')
@section('content')
 <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>Dashboard</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('admin.dashbord')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('user.index')}}">المسوقين</a></li>
                            <li class="breadcrumb-item active" aria-current="page">الصفحة</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                        <a href="javascript:void(0);" class="btn btn-sm btn-primary" title="">إضافة منتج</a>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-4 col-md-12">
                    <div class="card">
                        <div class="body">
                            <div class="media mb-0">
                                @if (Auth::user()->image)
                                  <img class="w60" src="{{url('images/admins/'.Auth::user()->image)}}" alt="">

                                @endif
                                <div class="media-body">
                                    <h5 class="m-0">{{Auth::user()->name}}</h5>
                                    <p class="text-muted mb-0">{{Auth::user()->email}}</p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <small class="text-muted">رقم الهوية</small>
                            <p class="mb-0">{{Auth::user()->numberid}}</p>
                            </li>
                            <li class="list-group-item">
                                <small class="text-muted">العنوان</small>
                            <p class="mb-0">{{Auth::user()->address}}</p>
                            </li>
                            <li class="list-group-item">
                                <small class="text-muted">العمر: </small>
                            <p class="mb-0">{{Auth::user()->age}}</p>
                            </li>

                        </ul>
                    </div>

                </div>
                <div class="col-lg-8 col-md-12">
                    <div class="card">
                        <div class="body">
                        <form method="POST" action="{{route('admin.update',[Auth::user()->id])}}" enctype="multipart/form-data">


                                                    @if ($errors->any())
                                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                            </div>
                                                @endif
                                                @if (session()->has('message'))
                                                                <div class="alert {{session()->get('status')}} alert-dismissible fade show"
                                                                    role="alert">
                                                                    <span> {{ session()->get('message') }}</span>
                                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                            @endif
                                                    @csrf
                                                    @method("PUT")
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                    <label style="font-size: 20px" > الإسم</label>
                                    <input type="text" name="name" value="{{Auth::user()->name}}" style="font-size: 20px" class="form-control" placeholder="الإسم">
                                    </div>
                                </div>
                                    <label style="font-size: 20px" > الصورة الشخصية</label>

                                         <div class="row clearfix">
                                            <div class="col-lg-12">
                                                <input type="file" width="590px" height="390px" class="dropify" name="image">
                                            </div>

                                        </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <label style="font-size: 20px" > رقم الهوية</label>
                                    <input type="number" value="{{Auth::user()->numberid}}" name="numberid" class="form-control" style="font-size: 20px" placeholder="رقم الهوية">
                                    </div>
                                </div>
                                    <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <label style="font-size: 20px" >رقم الهاتف</label>
                                    <input type="tel" name="mobile" class="form-control" style="font-size: 20px" value="{{Auth::user()->mobile}}" placeholder="رابط الفيسبوك">
                                    </div>
                                </div>

                                       <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <label style="font-size: 20px" > رابط الفيسبوك</label>
                                    <input type="text" name="urlfase" class="form-control" style="font-size: 20px" value="{{Auth::user()->urlfase}}" placeholder="رابط الفيسبوك">
                                    </div>
                                </div>
                                         <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <label style="font-size: 20px" > رابط الواتساب</label>
                                    <input type="text" name="urlwats" class="form-control" style="font-size: 20px" value="{{Auth::user()->urlwats}}" placeholder="رابط الواتساب">
                                    </div>
                                </div>
                                            <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <label style="font-size: 20px" > رابط الإنستغرام</label>
                                        <input type="text" name="urlinsta" class="form-control" style="font-size: 20px" value="{{Auth::user()->urlinsta}}" placeholder="رابط الإنستغرام">
                                    </div>
                                </div>


                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                            <label style="font-size: 20px" >العمر</label>

                                    <input type="text" class="form-control" style="font-size: 20px" value="{{Auth::user()->age}}" name="age" placeholder="العمر">
                                    </div>
                                </div>
                                       <div class="form-group">
                                <label style="font-size: 20px">الجنس</label>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input"  type="radio" id="Male"
                                        name="gender" value="Male"
                                        @if(Auth::user()->gender== 'Male') checked  @endif>
                                    <label  for="Male" class="custom-control-label" style="font-size: 20px">ذكر</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="Female"
                                        name="gender" value="Female"
                                        @if(Auth::user()->gender == 'Female') checked @endif>
                                    <label for="Female" class="custom-control-label" style="font-size: 20px">أنثى</label>
                                </div>
                            </div>

                                <div class="col-lg-12 col-md-12">

                                    <div class="form-group">
                                         <label style="font-size: 20px" >العنوان</label>

                                    <textarea rows="2" type="text" style="font-size: 20px" class="form-control" name="address" placeholder="العنوان">{{Auth::user()->address}}</textarea>
                                    </div>
                                </div>
                                 <div class="col-lg-12 col-md-12">

                                    <div class="form-group">
                                         <label style="font-size: 20px" >نبذة شخصية</label>

                                    <textarea rows="2" type="text" style="font-size: 20px" class="form-control" name="now" placeholder="نبذة شخصية">{{Auth::user()->now}}</textarea>
                                    </div>
                                </div>
                                    <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                    <input type="text" class="form-control" style="font-size: 20px" disabled value="{{Auth::user()->view_password}}">
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-round btn-primary">تعديل</button> &nbsp;&nbsp;
                        <a href="{{route('admin.dashbord')}}" type="button" class="btn btn-round btn-default">رجوع</a>
                        </form>
                    </div>
                    </div>
                    <div class="card">

                        <div class="body">
                        <form action="{{route('admin.password',[Auth::user()->id])}}" method="POST">

                            @csrf
                            @method("PUT")
                            <div class="row clearfix">

                                <div class="col-lg-12 col-md-12">
                                    <hr>
                                    <h6>تغيير كلمة السر</h6>

                                    <div class="form-group">
                                        <input type="password" name="password" class="form-control" placeholder="كلمة المرور الجديدة">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="confirm" class="form-control" placeholder="تأكيد كلمة المرور">
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-round btn-primary">تعديل</button> &nbsp;&nbsp;
                        <a  href="{{route('admin.dashbord')}}"  class="btn btn-round btn-default">رجوع</a>

                    </form>
                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
