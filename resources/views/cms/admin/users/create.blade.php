@extends('cms.admin.parent')
@section('title','Create User')

@section('content')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1 style="font-size: 20px">مسوق جديد</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin.dashbord')}}" style="font-size: 20px">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="{{route('user.index')}}" style="font-size: 20px">المسوقين</a></li>
                            <li class="breadcrumb-item active" aria-current="page"style="font-size: 20px">مسوق جديد</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                       {{-- <a href="https://themeforest.net/item/oculux-bootstrap-4x-admin-dashboard-clean-modern-ui-kit/23091507" class="btn btn-sm btn-success" title="Themeforest"><i class="icon-basket"></i> Buy Now</a> --}}
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="body">
                        <form method="POST" action="{{route('user.store')}}" enctype="multipart/form-data">


                            @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </div>
                        @endif
                        @if (session()->has('message'))
                                        <div class="alert {{session()->get('status')}} alert-dismissible fade show"
                                             role="alert">
                                            <span> {{ session()->get('message') }}</span>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                            @csrf
                                <div class="form-group row">
                                    <div class="col-md-10 col-sm-10">

                                        <div class="form-group">
                                            <label style="font-size: 20px" >الإسم</label>
                                        <input type="text" class="form-control" value="{{old('name')}}" style="font-size: 20px" name="name" placeholder=" ادخل الإسم رباعي ">
                                        </div>
                                        <div class="form-group">
                                            <label style="font-size: 20px" >رقم الهوية</label>
                                            <input type="number" class="form-control" value="{{old('numberid')}}" style="font-size: 20px" name="numberid"  placeholder="أدخل رقم الهوية">
                                        </div>
                                           <div class="form-group">
                                            <label style="font-size: 20px" > الإيميل</label>
                                            <input type="email" class="form-control" value="{{old('email')}}" style="font-size: 20px" name="email"  placeholder="الإيميل الشخصي">
                                        </div>
                                         <div class="form-group">
                                            <label style="font-size: 20px" >رقم الهاتف</label>
                                            <input type="tel" class="form-control" value="{{old('mobile')}}" style="font-size: 20px" name="mobile"  placeholder="أدخل رقم الهاتف">
                                        </div>
                                           <div class="form-group">
                                            <label style="font-size: 20px" >رقم الواتسآب</label>
                                            <input type="tel" class="form-control" value="{{old('now')}}" style="font-size: 20px" name="now"  placeholder="أدخل رقم الهاتف">
                                        </div>
                                         <div class="form-group">
                                            <label style="font-size: 20px" >العنوان</label>
                                            <input type="text" class="form-control" value="{{old('address')}}" style="font-size: 20px" name="address" placeholder="ادخل العنوان">
                                        </div>
                                         <div class="form-group">
                                            <label style="font-size: 20px" > العمر</label>
                                            <input type="number" class="form-control" value="{{old('age')}}" style="font-size: 20px" name="age" placeholder="أدخل العمر">
                                        </div>
                                         <div class="row clearfix">
                                            <div class="col-lg-12">
                                                <input type="file"  width="590px" height="390px" class="dropify" name="image">
                                            </div>

                                        </div>
                                          <div class="form-group">
                                            <label style="font-size: 20px" > كلمة السر</label>
                                            <input type="password" class="form-control" style="font-size: 20px" name="password" >
                                        </div>
                                          <div class="form-group">
                                            <label style="font-size: 20px" > تأكيد كلمة السر</label>
                                            <input type="password" class="form-control" style="font-size: 20px"  name="password2" >
                                        </div>
                                          <div class="form-group">
                                            <label style="font-size: 20px" >نسبة المسوق بالمئة</label>
                                            <input type="number" class="form-control" style="font-size: 20px" placeholder="وضع قيمة فقط مثال/ 70"  name="userprice" >
                                        </div>
                                    <div class="form-group">
                                <label style="font-size: 20px">الجنس</label>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input"  type="radio" id="Male"
                                        name="gender" value="Male"
                                        @if(old('gender') == 'Male') checked  @endif>
                                    <label style="font-size: 15px" for="Male" class="custom-control-label">ذكر</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="Female"
                                        name="gender" value="Female"
                                        @if(old('gender') == 'Female') checked @endif>
                                    <label style="font-size: 15px" for="Female" class="custom-control-label">أنثى</label>
                                </div>
                            </div>
                                            <div class="header">
                            <h2 style="font-size: 20px">الصلاحيات</h2>

                        </div>
                                    <div class="card">
                               <div class="body">
                            <div class="row clearfix">

                                <div class="col-12">
                                    <ul class="list-group mb-3 tp-setting" style="font-size: 20px">
                                        <li class="list-group-item">
                                                    رفع طلب
                                            <div class="float-right">
                                                <label class="switch">
                                                    <input name="create_order" @if((old('create_order')) == 'on') checked @endif id="create_order" type="checkbox" >
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                                    مشاهدة الطلبيات
                                            <div class="float-right">
                                                <label class="switch">
                                                    <input  name="index_order"  @if((old('index_order')) == 'on') checked @endif id="index_order" type="checkbox" >
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                                مشاهدة المنتجات
                                            <div class="float-right">
                                                <label class="switch">
                                                    <input name="product" @if((old('product')) == 'on') checked @endif id="product" type="checkbox"  >
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </li>
                                        <li class="list-group-item">

                                         مشاهدة الفئات
                                                     <div class="float-right">
                                                <label class="switch">
                                                    <input name="category" @if((old('category')) == 'on') checked @endif  id="category" type="checkbox"  >
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                            <div class="form-group">
                                <label style="font-size: 20px">حالة الحساب</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="status"
                                        id="status"
                                        @if((old('status')) == 'on') checked @endif>
                                    <label class="custom-control-label" style="font-size: 20px" for="status">مُفعل</label>

                        </div>
                    </div>

                                    </div>
                                </div>
                                <div>
                                            <button style="font-size: 20px" class="btn btn-sm btn-primary" type="submit"> تسجيل  </button>


                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
