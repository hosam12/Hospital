@extends('cms.admin.parent')
@section('title','مسوق')
@section('content')
 <div id="main-content">
            <div class="container">
                <div class="block-header">
                    <div class="row clearfix">
                        <div class="col-md-6 col-sm-12">
                            <h2>User Profile</h2>
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('admin.dashbord')}}" style="font-size: 20px">الرئيسية</a></li>
                                    <li class="breadcrumb-item"><a href="{{route('user.index')}}" style="font-size: 20px">المسوقين</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">{{$user->name}}</li>
                                </ol>
                            </nav>
                        </div>

                    </div>
                </div>
                <div class="row clearfix">

                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="card">
                            <div class="header">
                                <ul class="header-dropdown dropdown">
                                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>

                                </ul>
                            </div>
                            <div class="body">
                                 <div class="col-md-12">
                        <div class="card social">
                            <div class="profile-header d-flex justify-content-between justify-content-center">
                                <div class="d-flex">
                                    <div class="mr-3">
                                    <img src="{{url('images/users/'.$user->image)}}" alt="{{$user->name}}" class="rounded" alt="">
                                    </div>
                                    <div class="details">
                                        <h5 class="mb-0">{{$user->name}}</h5>
                                        <span class="text-light">الربح الحالي</span>
                                        <h5  class="mb-0">{{$user->mall}}</h5>
                                        <p class="mb-0"><span style="font-size: 20px">النسبة: <strong>% {{$user->userprice}} </strong></p>
                                    </div>
                                </div>
                                <div>

                                <a href="{{route('user.edit',[$user->id])}}" class="btn btn-primary btn-sm">تعديل</a>
                                <a href="{{route('user.details',[$user->id])}}" class="btn btn-success btn-sm">كافة الأوامر</a>
                                <a href="{{route('user.month',[$user->id])}}" class="btn btn-info btn-sm">الطلبات الشهرية</a>
                                <a href="{{route('select.user',[$user->id])}}" class="btn btn-warning btn-sm">إرسال رسالة</a>

                                </div>
                            </div>
                        </div>
                    </div>
                                <hr>
                                <small class="text-muted">Email address: </small>
                                <p>{{$user->email}}</p>
                                <hr>
                                <small class="text-muted">Mobile: </small>
                                <p>{{$user->mobile}}</p>
                                <hr>
                                <small class="text-muted">WhatsApp: </small>
                                <p>{{$user->now}}</p>
                                <hr>
                                <small class="text-muted">Age: </small>
                                <p class="m-b-0">{{$user->age}}</p>
                                <hr>

                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="card">
                            <div class="header">
                                <h2 style="font-size: 20px">الصلاحيات</h2>
                                <ul class="header-dropdown dropdown">
                                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>

                                </ul>
                            </div>
                              <form method="POST" action="{{route('setting.update',[$user->id])}}">
                                @csrf
                                @method("PUT")
                            <div class="body">
                               <div class="row clearfix">

                                <div class="col-12">
                                    <ul class="list-group mb-3 tp-setting" style="font-size: 20px">
                                        <li class="list-group-item">
                                                    رفع طلب
                                            <div class="float-right">
                                                <label class="switch">
                                                    <input name="create_order" id="create_order" type="checkbox" @if($user->setting->create_order == 'Visible')checked @endif) >
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                                    مشاهدة الطلبيات
                                            <div class="float-right">
                                                <label class="switch">
                                                    <input  name="index_order" @if($user->setting->index_order == 'Visible') checked @endif  id="index_order" type="checkbox">
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                                مشاهدة المنتجات
                                            <div class="float-right">
                                                <label class="switch">
                                                    <input name="product" id="product" @if($user->setting->product == 'Visible') checked @endif type="checkbox" >
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </li>
                                        <li class="list-group-item">

                                         مشاهدة الفئات
                                                     <div class="float-right">
                                                <label class="switch">
                                                    <input name="category" id="category" @if($user->setting->category == 'Visible') checked @endif type="checkbox" >
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </li>

                                        <li class="list-group-item">

                                         التواصل معنا
                                                     <div class="float-right">
                                                <label class="switch">
                                                    <input name="vedio" id="vedio" @if($user->setting->vedio == 'Visible') checked @endif type="checkbox" >
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </li>
                                            <li class="list-group-item">

                                         حالة الحساب
                                           <div class="float-right">
                                                <label class="switch">
                                                    <input name="status" id="status" @if($user->status== 'Active') checked @endif type="checkbox" >
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>



                            <button type="submit"  class="btn btn-round btn-primary">تعديل </a> &nbsp;&nbsp;

                            </form>


                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>
@endsection
