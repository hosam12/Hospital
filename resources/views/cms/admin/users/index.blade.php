@extends('cms.admin.parent')
@section('title','المسوقين')

@section('content')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin.dashbord')}}" style="font-size: 20px">الرئيسية</a></li>
                            <li class="breadcrumb-item " style="font-size: 20px" aria-current="page">المسوقين</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{route('user.create')}}" class="btn btn-sm btn-primary" title="">مسوق جديد</a>
                        {{-- <a href="https://themeforest.net/item/oculux-bootstrap-4x-admin-dashboard-clean-modern-ui-kit/23091507" class="btn btn-sm btn-success" title="Themeforest"><i class="icon-basket"></i> Buy Now</a> --}}
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2 style="font-size: 20px">المسوقين</h2>

                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th style="font-size: 17px">مشاهدة</th>
                                        <th style="font-size: 17px">الصورة</th>
                                        <th style="font-size: 17px"> رقم الهوية</th>
                                        <th style="font-size: 17px">الإسم</th>
                                        <th style="font-size: 17px">الإيميل</th>
                                        <th style="font-size: 17px">الطلبات</th>
                                        <th style="font-size: 17px">نسبة المسوق</th>
                                        <th style="font-size: 17px">العمر</th>
                                        <th style="font-size: 17px">العنوان</th>
                                        <th style="font-size: 17px">الهاتف</th>
                                        <th style="font-size: 17px">الواتسآب</th>
                                        <th style="font-size: 17px">الحالة الإجتماعية</th>
                                        <th style="font-size: 17px">حالة الحساب</th>
                                        <th style="font-size: 17px">الانشاء بواسطة</th>
                                        <th style="font-size: 17px"> تاريخ الإنشاء</th>

                                        <th style="font-size: 17px">الإعدادات</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <span hidden>{{$i=0}}</span>
                                    @foreach ($users as $user)
                                <span hidden>{{$i++}}</span>

                                    <tr>
                                    <td>{{$i}}</td>
                                    <td>
                                    <a href="{{route('user.show',[$user->id])}}"><span style="font-size: 17px" class="badge badge-warning"><i class="icon-user"></i> عرض</span></a>   </td>
                                        <td class="w60">
                                            <img src="{{url('images/users/'.$user->image)}}" data-toggle="tooltip" data-placement="top" title="Avatar Name" alt="Avatar" class="w35 h35 rounded">
                                        </td>
                                        <td>
                                            <div class="font-15">{{$user->numberid}}</div>
                                        </td>
                                    <td><span style="font-size: 17px">{{$user->name}}</span></td>
                                    <td><span style="font-size: 17px">{{$user->email}}</span></td>
                                       <td>
                                            <a class="btn btn-info btn-sm"
                                               href="{{route('user.details',[$user->id])}}">
                                                <i class="icon-note">
                                                </i>
                                                ({{$user->details_count}}) عدد الطلبات
                                            </a>
                                            <span class="badge badge-dark"></span>
                                        </td>
                                     <td><span style="font-size: 17px">{{$user->userprice}} %</span></td>

                                    <td><span style="font-size: 17px">{{$user->age}}</span></td>
                                    <td><span style="font-size: 17px">{{$user->adress}}</span></td>
                                    <td><span style="font-size: 17px">{{$user->mobile}}</span></td>
                                    <td><span style="font-size: 17px">{{$user->now}}</span></td>
                                    <td><span style="font-size: 17px">{{$user->gender}}</span></td>
                                     <td>
                                         @if($user->status=='Active')
                                        <span style="font-size: 17px" class="badge badge-success">نشط</span>
                                        @else
                                    <span style="font-size: 17px" class="badge badge-danger">متوقف</span>
                                        @endif

                                    </td>
                                    @if($user->admin_id)
                                    <td><span>{{$user->admin->name}}</span></td>
                                    @else
                                    <td><span>التسجيل عبرالموقع</span></td>
                                    @endif
                                    <td><span>{{$user->created_at}}</span></td>

                                        <td>
                                        <a href="{{route('user.edit',[$user->id])}}" type="button" style="font-size: 20px" class="btn btn-sm btn-default" title="تعديل"><i class="fa fa-edit"></i> تعديل</a>
                                        {{-- <a onclick="confirmDelete(this, '{{$user->id}}')" type="button" style="font-size: 20px" class="btn btn-sm btn-default js-sweetalert" title="حذف" data-type="confirm"><i class="fa fa-trash-o text-danger"> حذف </i></a> --}}
                                        {{-- <button type="button" class="btn btn-sm btn-default" title="عرض"><i class="icon-diamond"></i></button> --}}

                                        </td>




                                        {{-- <td>
                                            <a href="javascript:void(0);" class="btn btn-info btn-round">Interview</a>

                                        </td> --}}
                                    </tr>
                                         @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @endsection
{{-- @endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script>
        function confirmDelete(app, id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    deleteuser(app, id)
                }
            })
        }

        function deleteuser(app, id) {
            axios.delete('/cms/admin/user/' + id)
                .then(function (response) {
                    // handle success (Status Code: 200)
                    console.log(response);
                    console.log(response.data);
                    showMessage(response.data);
                    app.closest('tr').remove();
                })
                .catch(function (error) {
                    // handle error (Status Code: 400)
                    console.log(error);
                    console.log(error.response.data);
                    showMessage(error.response.data);
                })
                .then(function () {
                    // always executed
                });
        }

        function showMessage(data) {
            Swal.fire({
                position: 'center',
                icon: data.icon,
                title: data.title,
                showConfirmButton: false,
                timer: 1500
            })
        }
    </script>

@endsection --}}
