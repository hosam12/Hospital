@extends('cms.admin.parent')
@section('title','منتج جديد')
@section('content')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1 style="font-size: 20px">منتج جديد</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin.dashbord')}}" style="font-size: 20px">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="{{route('product.index')}}" style="font-size: 20px">المنتجات</a></li>
                            <li class="breadcrumb-item active" aria-current="page"style="font-size: 20px">منتج جديد</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                       {{-- <a href="https://themeforest.net/item/oculux-bootstrap-4x-admin-dashboard-clean-modern-ui-kit/23091507" class="btn btn-sm btn-success" title="Themeforest"><i class="icon-basket"></i> Buy Now</a> --}}
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="body">
                        <form method="POST" action="{{route('product.store')}}" enctype="multipart/form-data">


                            @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </div>
                        @endif
                        @if (session()->has('message'))
                                        <div class="alert {{session()->get('status')}} alert-dismissible fade show"
                                             role="alert">
                                            <span> {{ session()->get('message') }}</span>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                            @csrf
                                <div class="form-group row">
                                    <div class="col-md-10 col-sm-10">

                                        <div class="form-group">
                                            <label style="font-size: 20px" >إسم المنتج</label>
                                            <input type="text" class="form-control" value="{{old('name')}}" style="font-size: 20px" name="name" placeholder=" اسم المنتج ">
                                        </div>
                                        <div class="form-group">
                                            <label style="font-size: 20px" >كود المنتج</label>
                                            <input type="text" class="form-control" value="{{old('code')}}" style="font-size: 20px" name="code"  placeholder="كود المنتج">
                                        </div>
                                <div class="form-group">
                                            <label style="font-size: 20px">فئة المنتج</label>
                                             <div class="col-md-12 col-sm-10">
                                             <select class="form-control input-height" name="category" >
                                            <option value="">Select...</option>
                                           @foreach ($cate as $item)
                                        <option style="font-size: 20px" @if(old('category')==$item->id) selected @endif value="{{$item->id}}">{{$item->name}}</option>

                                           @endforeach
                                        </select>
                                    </div>
                                </div>
                                    <div class="col-lg-12 col-md-12">
                                        <label style="font-size: 20px" > وصف المنتج </label>

                                        <div class="form-group">
                                        <textarea rows="2" type="text" name="desc" style="font-size: 20px"  id="desc" class="form-control" placeholder=" الوصف العام عن المنتج">{{old('desc')}}</textarea>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                            <label style="font-size: 20px" >سعر البيع من التاجر</label>
                                            <input type="number" class="form-control" value="{{old('realprice')}}" style="font-size: 20px" name="realprice"  placeholder="السعر بالجملة">
                                        </div>
                                        <div class="form-group">
                                            <label style="font-size: 20px" >الحد الأدنى</label>
                                            <input type="number" class="form-control" value="{{old('from')}}" style="font-size: 20px" name="from" placeholder="من">
                                        </div>

                                        <div class="form-group">
                                            <label style="font-size: 20px" > الحد الأقصى</label>
                                            <input type="number" class="form-control" value="{{old('to')}}" style="font-size: 20px" name="to" placeholder="إلى">
                                        </div>

                                           <div class="form-group"  >
                                            <label style="font-size: 20px" >   السعر الثابت</label>
                                        <input type="number" class="form-control" value="{{old('staticprice')}}" style="font-size: 20px" name="staticprice" placeholder="السعر الثابت">
                                        </div>




                                          <div class="form-group">
                                            <label style="font-size: 20px" >الألوان المتوفرة</label>
                                            <input type="text" class="form-control" value="{{old('color')}}" style="font-size: 20px" name="color"  placeholder="الألوان">
                                        </div>


                                        <div class="row clearfix">
                                            <div class="col-lg-12">
                                                 <label style="font-size: 20px" > صور المنتج</label>
                                                <input type="file" multiple   width="590px" height="390px" class="dropify" name="image[]">
                                            </div>

                                        </div>



                                 <div class="form-group">
                                <label style="font-size: 20px">المنتج متوفر</label>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input"  type="radio" id="Male"
                                        name="gender" value="Male"
                                        @if(old('gender') == 'Male') checked  @endif>
                                    <label style="font-size: 15px" for="Male" class="custom-control-label">ذكور</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="Female"
                                        name="gender" value="Female"
                                        @if(old('gender') == 'Female') checked @endif>
                                    <label style="font-size: 15px" for="Female" class="custom-control-label">إناث</label>
                                </div>
                                  <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="MaleFemale"
                                        name="gender" value="MaleFemale"
                                        @if(old('gender') == 'MaleFemale') checked @endif>
                                    <label style="font-size: 15px" for="MaleFemale" class="custom-control-label">الجميع</label>
                                </div>
                            </div>



                            <div class="form-group">
                                <label style="font-size: 20px"> تفعيل المنتج</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="status"
                                        id="status"
                                        @if((old('status')) == 'on') checked @endif>
                                    <label class="custom-control-label" style="font-size: 20px" for="status">مُفعل</label>

                        </div>
                    </div>
                                                        <div class="col-lg-12 col-md-12">
                                        <label style="font-size: 20px" > أيةَ مُلاحظة </label>

                                        <div class="form-group">
                                        <textarea rows="2" type="text" name="note"  id="note" class="form-control" placeholder="ملاحظات أُخرى">{{old('note')}}</textarea>
                                        </div>
                                    </div>

                                    </div>
                                </div>
                                <div>
                                            <button style="font-size: 20px" class="btn btn-sm btn-primary" type="submit"> تسجيل  </button>


                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
