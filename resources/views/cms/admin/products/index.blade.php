@extends('cms.admin.parent')
@section('title','المنتجات')

@section('content')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin.dashbord')}}" style="font-size: 20px">الرئيسية</a></li>
                            <li class="breadcrumb-item " style="font-size: 20px" aria-current="page">المنتجات</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{route('product.create')}}" class="btn btn-sm btn-primary" title="">منتج جديد</a>
                        {{-- <a href="https://themeforest.net/item/oculux-bootstrap-4x-admin-dashboard-clean-modern-ui-kit/23091507" class="btn btn-sm btn-success" title="Themeforest"><i class="icon-basket"></i> Buy Now</a> --}}
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2 style="font-size: 20px">المنتجات</h2>

                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th style="font-size: 17px">مشاهدة</th>
                                        {{-- <th style="font-size: 17px">الصورة</th> --}}
                                        <th style="font-size: 17px">اسم المنتج</th>
                                        <th style="font-size: 17px"># الكود</th>
                                        <th style="font-size: 17px">الفئة</th>
                                        <th style="font-size: 17px">سعر الجملة</th>
                                        <th style="font-size: 17px">الحد الأدنى</th>
                                        <th style="font-size: 17px">الحد الأقصى</th>
                                        <th style="font-size: 17px"> السعر الثابت</th>
                                        <th style="font-size: 17px">جنس المنتج</th>
                                        <th style="font-size: 17px">الألوان المتوفرة</th>
                                        <th style="font-size: 17px">حالة المنتج</th>
                                        <th style="font-size: 17px"> تاريخ الإنشاء</th>
                                        <th style="font-size: 17px">الإعدادات</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <span hidden>{{$i=0}}</span>
                                    @foreach ($products as $product)
                                <span hidden>{{$i++}}</span>

                                    <tr>
                                    <td>{{$i}}</td>
                                    <td>
                                    <a href="{{route('product.edit',[$product->id])}}"><span style="font-size: 17px" class="badge badge-warning"><i class="icon-user"></i> عرض</span></a>
                                 </td>
                                        {{-- <td class="w60">
                                            <img src="{{url('images/users/'.$user->image)}}" data-toggle="tooltip" data-placement="top" title="Avatar Name" alt="Avatar" class="w35 h35 rounded">
                                        </td> --}}
                                        <td>
                                            <div class="font-15">{{$product->name}}</div>
                                        </td>
                                    <td><span style="font-size: 17px" >{{$product->code}}</span></td>
                                    <td><span style="font-size: 17px">{{$product->category->name}}</span></td>
                                     <td><span style="font-size: 17px">{{$product->realprice}}</span></td>

                                    <td>
                                        @if($product->from)
                                         <span  class="badge badge-success" style="font-size: 17px">{{$product->from}}</span>
                                         @else
                                        <span  class="badge badge-danger" style="font-size: 17px">السعر ثابت</span>

                                        @endif



                                    </td>
                                    <td>
                                                         @if($product->to)
                                         <span  class="badge badge-success" style="font-size: 17px">{{$product->to}}</span>
                                         @else
                                        <span  class="badge badge-danger" style="font-size: 17px">السعر ثابت</span>

                                        @endif
                                    </td>
                                    <td><span style="font-size: 17px">

                                                         @if($product->staticprice)
                                         <span  class="badge badge-success" style="font-size: 17px">{{$product->staticprice}}</span>
                                         @else
                                        <span  class="badge badge-danger" style="font-size: 17px">السعر مُتغير</span>

                                        @endif
                                    </span></td>
                                    <td><span style="font-size: 17px">

                                       @if ($product->gender=='Male')
                                           <span style="font-size: 17px"> ذكور</span>


                                           @elseif($product->gender=='Female')
                                               <span style="font-size: 17px"> إناث</span>
                                          @else
                                             <span style="font-size: 17px"> ذكور وإناث</span>

                                       @endif


                                    </span></td>
                                    <td>
                                        @if($product->color)
                                        <span style="font-size: 17px">{{ Str::limit($product->color,30)}}</span>
                                        @else
                                        <span style="font-size: 17px" class="badge badge-warning">---</span>
                                        @endif

                                    </td>
                                     <td>
                                         @if($product->status=='Visible')
                                        <span style="font-size: 17px" class="badge badge-success">فعّال</span>
                                        @else
                                    <span style="font-size: 17px" class="badge badge-danger">متوقف</span>
                                        @endif

                                    </td>

                                    <td><span>{{$product->created_at->diffForHumans()}}</span></td>

                                        <td>
                                        <a href="{{route('product.edit',[$product->id])}}" type="button" style="font-size: 20px" class="btn btn-sm btn-default" title="تعديل"><i class="fa fa-edit"></i> تعديل</a>
                                        <a onclick="confirmDelete(this, '{{$product->id}}')" type="button" style="font-size: 20px" class="btn btn-sm btn-default js-sweetalert" title="حذف" data-type="confirm"><i class="fa fa-trash-o text-danger"> حذف </i></a>
                                        {{-- <button type="button" class="btn btn-sm btn-default" title="عرض"><i class="icon-diamond"></i></button> --}}

                                        </td>




                                        {{-- <td>
                                            <a href="javascript:void(0);" class="btn btn-info btn-round">Interview</a>

                                        </td> --}}
                                    </tr>
                                         @endforeach
                                </tbody>
                            </table>
                        </div>
                         <div>
                                {{$products->render()}}
                            </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script>
        function confirmDelete(app, id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    deleteproduct(app, id)
                }
            })
        }

        function deleteproduct(app, id) {
            axios.delete('/product/' + id)
                .then(function (response) {
                    // handle success (Status Code: 200)
                    console.log(response);
                    console.log(response.data);
                    showMessage(response.data);
                    app.closest('tr').remove();
                })
                .catch(function (error) {
                    // handle error (Status Code: 400)
                    console.log(error);
                    console.log(error.response.data);
                    showMessage(error.response.data);
                })
                .then(function () {
                    // always executed
                });
        }

        function showMessage(data) {
            Swal.fire({
                position: 'center',
                icon: data.icon,
                title: data.title,
                showConfirmButton: false,
                timer: 1500
            })
        }
    </script>

@endsection
