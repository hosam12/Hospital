<!DOCTYPE html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>:: تكنو باند للتسويق الإلكتروني</title>
<link rel="icon" href="{{asset('cms/favicon.ico')}}" type="image/x-icon">

<!-- Core css file -->
<link rel="stylesheet" type="text/css" href="{{asset('cms/web/assets/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('cms/web/assets/fonts/line-icons.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/animate.css')}}">

<!-- Plugins CSS file -->
<link rel="stylesheet" type="text/css" href="{{asset('cms/web/assets/css/slicknav.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('cms/web/assets/css/owl.carousel.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('cms/web/assets/css/owl.theme.css')}}">

<!-- Project css with  Responsive-->
<link rel="stylesheet" type="text/css" href="{{asset('cms/web/assets/css/main.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('cms/wem/assets/css/responsive.css')}}">

</head>
<body>
<!-- Header Area wrapper Starts -->
<header id="header-wrap">
    <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar indigo">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar"
                    aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    <span class="icon-menu"></span>
                    <span class="icon-menu"></span>
                    <span class="icon-menu"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="main-navbar">
                <ul class="navbar-nav mr-auto w-100 justify-content-end clearfix">
                    <li class="nav-item active"><a class="nav-link" href="#hero-area">الصفحة الرئيسية</a></li>
                    <li class="nav-item"><a class="nav-link" href="#services">خدماتنا</a></li>
                    <li class="nav-item"><a class="nav-link" href="#feature">حول الشركة</a></li>
                    <li class="nav-item"><a class="nav-link" href="#team">فريق العمل</a></li>
                    <li class="nav-item"><a class="nav-link" href="#footer">تواصل معنا</a></li>
                </ul>
                <div class="btn-sing float-right">
                    @guest
                        @else
                    <a class="btn btn-border" href="{{route('user.profile')}}">الصفحة الشخصية</a>

                    @endguest
                </div>
            </div>
        </div>

        <ul class="mobile-menu navbar-nav">
            <li><a class="page-scroll" href="#hero-area">الصفحة الرئيسية</a></li>
            <li><a class="page-scroll" href="#services">خدماتنا</a></li>
            <li><a class="page-scroll" href="#feature">حول الشركة</a></li>
            <li><a class="page-scroll" href="#team">فريق العمل</a></li>
            <li><a class="page-scroll" href="#footer">تواصل معنا</a></li>
        </ul>

    </nav>

    <div id="hero-area" class="hero-area-bg particles_js">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="contents text-center">
                        <h2 class="head-title wow fadeInUp">شركة تكنو باند للتسويق الإلكتروني <br>
                            @guest

                            @else
                             {{Auth::user()->name}}
                            @endguest

                        </h2>
                        <div class="header-button wow fadeInUp" data-wow-delay="0.3s">
                            @guest
                        <a href="{{route('user.login.view')}}" class="btn btn-common">تسجيل دخول</a>
                            <a href="{{route('reg.user.view')}}" class="btn btn-common blush">إشتراك</a>
                            @else
                               <a href="#" class="btn btn-common">تسجيل خروج</a>

                            @endguest

                        </div>
                    </div>
                    <div class="img-thumb text-center wow fadeInUp" data-wow-delay="0.6s">
                        <img class="img-fluid" src="{{url('cms/web/assets/img/hero-1.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div id="particles-js"></div>
    </div>
    <!-- Hero Area End -->

</header>

<!-- Services Section Start -->
<section id="services" class="section-padding">
    <div class="container">
        <div class="section-header text-center wow fadeInDown" data-wow-delay="0.3s">
            <h2 class="section-title">خدماتنا</h2>

        </div>
        <div class="row">

            <div class="col-md-6 col-lg-4 col-xs-12">
                <div class="services-item wow fadeInUp" data-wow-delay="1.2s">
                    <div class="icon">
                        <i class="lni-mobile"></i>
                    </div>
                    <div class="services-content">
                        <h3><a href="#">تصميم وتطوير تطبيقات الويب</a></h3>
                        <p>طلاب وخريجين هندسة حاسوب</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 col-xs-12">
                <div class="services-item wow fadeInUp" data-wow-delay="1.5s">
                    <div class="icon">
                        <i class="lni-briefcase"></i>
                    </div>
                    <div class="services-content">
                        <h3><a href="#">العلامات التجارية </a></h3>
                        <p>نعمل على إشهار العلامات التجارية المدفونة ذات الجودة العالية</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 col-xs-12">
                <div class="services-item wow fadeInUp" data-wow-delay="1.8s">
                    <div class="icon">
                        <i class="lni-bar-chart"></i>
                    </div>
                    <div class="services-content">
                        <h3><a href="#">تسويق رقمي</a></h3>
                        <p>نعمل على تسهيل بيع المنتجات وتوفير ربح للمتجر وللمسوقين</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Feature Section Start -->
<div id="feature" class="section-padding">
    <div class="container">
        <div class="row mb-5 vertical-content">
            <div class="col-lg-6 col-md-12">
                {{-- <div class="wow fadeInLeft mb-3" data-wow-delay="0.3s">
                    <h2 class="title-h3">تكنو باند</h2>
                    <p>شركة متخصصة في تسويق المنتجات لكافة المتاجر العالمية  .</p><br>
                </div> --}}
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <h2>تكنو باند</h2>
                        <div class="features-box wow fadeInLeft" data-wow-delay="0.3s">
                            <div class="features-icon"><i class="lni-layers"></i></div>
                            <div class="features-content">
                    <p>شركة متخصصة في تسويق المنتجات لكافة المتاجر العالمية  .</p><br>
                    <p>تم انشاؤها في عام 2020 من قِبل طلاب في جامعة فلسطين من كلية تكنولوجيا المعلومات </p>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="mt-3 features_img wow fadeInRight" data-wow-delay="0.7s">
                    <img src="{{asset('cms/assets/images/image-gallery/11.jpg')}}" alt="" class="img-fluid rounded mx-auto d-block">
                </div>
            </div>
        </div>
        <hr>
        <div class="row mt-5 vertical-content">
            <div class="col-lg-6 col-md-12">
                <div class="mt-3 features_img wow fadeInRight" data-wow-delay="0.7s">
                    <img src="{{asset('cms/assets/images/image-gallery/2.jpg')}}" alt="" class="img-fluid rounded mx-auto d-block">
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="wow fadeInLeft mb-3" data-wow-delay="0.3s">
                    <h2 class="title-h3">ابدأ معنا بتحقيق  الحلم</h2>
                    <p></p>
                </div>
                <div class="row">

                    <div class="col-md-6 col-sm-6">
                        <div class="features-box wow fadeInLeft" data-wow-delay="1.2s">
                            <div class="features-icon"><i class="lni-leaf"></i></div>
                            <div class="features-content">
                                <h4>تعاهدنا أن نكون معاً وحتماً سنصل</h4>
                                <p>فريق تكنو باند</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Team Section Start -->
<section id="team" class="section-padding text-center">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-header text-center wow fadeInDown" data-wow-delay="0.3s">
                    <h2 class="section-title"> فريق العمل</h2>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($admins as $item)
                 <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="team-item text-center">
                    <div class="team-img">
                    <img class="img-fluid" style="height: 250px" style="width: 380px" src="{{url('images/admins/'.$item->image)}}" alt="{{$item->name}}">
                        <div class="team-overlay">
                            <div class="overlay-social-icon text-center">
                                <ul class="social-icons">
                                    <li><a href="{{$item->urlfase}}"><i class="lni-facebook-filled" aria-hidden="true"></i></a></li>
                                    {{-- <li><a href="#"><i class="lni-twitter-filled" aria-hidden="true"></i></a></li> --}}
                                <li><a href="{{$item->urlinsta}}" target="blank"><i class="lni-instagram-filled" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="info-text">
                    <h3><a href="{{$item->urlinsta}}" target="blank">{{$item->name}} </a></h3>
                    <p>{{$item->now}}</p>
                    </div>
                </div>
            </div>
            @endforeach


        </div>
    </div>
</section>

<!-- Clients Section Start -->
                        <h2 style="text-align: center">الماركات المسجلة</h2>

<div id="clients" class="section-padding">

    <div class="container">

        <div class="row text-align-">
            @foreach ($companys as $item)


            <div class="col-lg-3 col-md-3 col-xs-12 wow fadeInUp" data-wow-delay="0.3s">
                <div class="client-item-wrapper">
                <img  class="img-fluid" style="width: 480px" style="height: 240px" src="{{url('images/companys/'.$item->image)}}" alt="{{$item->name}}">
            </div>

        </div>
           @endforeach
    </div>
</div>






<!-- Footer Section Start -->
<footer id="footer" class="footer-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.2s">

                <p style="font-size: 17px">شركة متخصصة في تسويق المنتجات لكافة المتاجر العالمية  </p>
                <p style="font-size: 17px">تهدف الشركة لتوفير فرص عمل للشباب العاطلين عن العمل من خلال التسويق الإلكتروني </p>
                <h2 style="color: white">العمل مفتوح للجميع بلا استثناء  </h2>
                <h4>استغل وقتك وتواصل  من خلال الروابط المرفقة </h4>

            </div>
            <div class="col-lg-2 col-md-6 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
                <h3 class="footer-titel">العلامات التجارية</h3>
                <ul>
                    @foreach ($companys as $item)
                    <li><a href="#">{{$item->name}}</a></li>

                    @endforeach

                </ul>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.8s">
                <h3 class="footer-titel">تواصل معنا</h3>
                <div class="social-icon">
                    <a class="instagram" href="https://www.instagram.com/techno._.band/"><i class="lni-instagram-filled"></i></a>
                 </div>
                                    <a href="https://wa.me/970598401856" target="blank">+ 970 598 401 856 </a><br>
                                    <a  href="https://wa.me/970597753024" target="blank" >+ 970 597 753 024 </a><br>


            </div>
        </div>
    </div>
</footer>

<section id="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>مطور الموقع ©حسام الدين ماهر عطا ابو سكران</p>
            </div>
            <div class="col-md-12">
                <p>الحقوق محفوظة   2020 hm0592852026@gmail.com    </p>
            </div>
        </div>
    </div>
</section>

<!-- Go to Top Link -->
<a href="#" class="back-to-top"><i class="lni-arrow-up"></i></a>

<!-- Preloader -->
<div id="preloader">
    <div class="loader">
        <img src="{{asset('cms/assets/images/icon.svg')}}" width="40" height="40" alt="Oculux">
        <p>الرجاء الإنتظار</p>
    </div>
</div>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{asset('cms/web/assets/js/jquery-min.js')}}"></script>
<script src="{{asset('cms/web')}} assets/js/popper.min.js"></script>
<script src="{{asset('cms/web')}}assets/js/bootstrap.min.js"></script>

<script src="{{asset('cms/web/assets/js/wow.js')}}"></script>
<script src="{{asset('cms/web/assets/js/scrolling-nav.js')}}"></script>
<script src="{{asset('cms/web/assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('cms/web/assets/js/jquery.nav.js')}}"></script>
<script src="{{asset('cms/web/assets/js/jquery.easing.min.js')}}"></script>
<script src="{{asset('cms/web/assets/js/jquery.slicknav.js')}}"></script>
<script src="{{asset('cms/web/assets/js/particles.min.js')}}"></script>

<script src="{{asset('cms/web/assets/js/main.js')}}"></script>
<script src="{{asset('cms/web/assets/js/form-validator.min.js')}}"></script>
<script src="{{asset('cms/web/assets/js/contact-form-script.min.js')}}"></script>
<script src="{{asset('cms/web/assets/js/particlesjs.js')}}"></script>
</body>
</html>
