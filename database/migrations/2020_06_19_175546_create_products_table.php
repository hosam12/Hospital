<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('desc',255)->nullable();
            $table->string('color',255)->nullable();
            $table->string('code')->unique();
            $table->double('realprice');
            $table->double('from')->nullable();
            $table->double('to')->nullable();
            $table->double('staticprice')->nullable();
            $table->enum('gender', ['Male', 'Female','MaleFemale'])->default('Male')->nullable();
            $table->enum('status', ['Visible', 'InVisible'])->default('Visible');
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories')->cascadeOnDelete();
            $table->string('note')->nullable();

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
