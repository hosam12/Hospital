<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
           $table->id();
           $table->string('name');
           $table->string('email')->unique();
           $table->integer('age');
           $table->integer('numberid')->unique();
           $table->string('mobile')->unique();
           $table->enum('gender', ['Male', 'Female'])->default('Male');
            $table->enum('status',['Active','Blocked'])->default('Active');
            $table->string('urlfase')->nullable();
            $table->string('urlwats')->nullable();
            $table->string('urlinsta')->nullable();
            $table->string('address')->nullable();
           $table->string('image',255)->nullable();
           $table->string('now')->nullable();
           $table->string('password');
           $table->string('view_password');
           $table->timestamp('email_verified_at')->nullable();
           $table->rememberToken();
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
