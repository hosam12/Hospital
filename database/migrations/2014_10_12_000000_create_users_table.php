<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->integer('dis')->default(0)->nullable();
            $table->integer('age');
            $table->integer('numberid')->unique()->nullable();
            $table->string('mobile')->unique();
            $table->string('adress');
            $table->enum('gender', ['Male', 'Female'])->default('Male');
            $table->enum('status',['Active','Blocked'])->default('Active');
            $table->string('image',255)->nullable();
            $table->string('now')->nullable();
            $table->double('userprice')->default(0);
            $table->string('password');
            $table->string('view_password');
            $table->unsignedBigInteger('general_id');
            $table->double('mall')->nullable()->default(0);
            $table->foreign('general_id')->references('id')->on('generals');
            $table->unsignedBigInteger('admin_id')->nullable();
            $table->foreign('admin_id')->references('id')->on('admins');
            $table->timestamp('email_verified_at')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
