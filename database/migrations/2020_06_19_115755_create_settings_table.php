<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();

                     $table->enum('create_order',['Visible','InVisible'])->default('Visible');
                    $table->enum('index_order',['Visible','InVisible'])->default('Visible');
                    $table->enum('product',['Visible','InVisible'])->default('Visible');
                    $table->enum('category',['Visible','InVisible'])->default('Visible');
                    $table->enum('status',['Visible','InVisible'])->default('Visible');
                    $table->enum('vedio',['Visible',['InVisible']])->default('Visible');
                    $table->unsignedBigInteger('user_id');
                    $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
